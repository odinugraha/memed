<?php
/*
 * Langguage mapping for Role
 */

return array( 
       "form"=>array(
                    array(
                        'id'    => 'doc_schId',
                        'name'  => 'KODE',
                        'type'  => 'hidden',
                        'readonly'  => '0',
                        ),
                    array(
                        'id'    => 'doc_schNumb',
                        'name'  => 'Nomor Faskes',
                        'type'  => 'autocomplete',
                        'type'  => 'text',
                        'readonly'  => '0',
                        ),
                    array(
                        'id'    => 'doc_schName',
                        'name'  => 'Nama Dokter',
                        'type'  => 'autocomplete',
                        'type'  => 'text',
                        'readonly'  => '0',
                        ),
                    array(
                        'id'    => 'doc_schPoly',
                        'name'  => 'Nama Poli',
                        'type'  => 'autocomplete',
                        'comboapi'  => 'backend/public/api/admin/master/Polyhostcombo',
                        'readonly'  => '0',
                        ),
                    array(
                        'id'    => 'doc_schDay',
                        'name'  => 'Hari',
                        'type'  => 'text',
                        'readonly'  => '0',
                        ),
                    array(
                        'id'    => 'doc_schPeriodStart',
                        'name'  => 'Jam Awal',
                        'type'  => 'text',
                        'readonly'  => '0',
                        ),
                    array(
                        'id'    => 'doc_schPeriodStop',
                        'name'  => 'Jam Akhir',
                        'type'  => 'text',
                        'readonly'  => '0',
                    ),
                        
                )
);
