/*
SQLyog Professional v12.5.1 (64 bit)
MySQL - 10.1.37-MariaDB : Database - memed
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`memed` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `memed`;

/*Table structure for table `admin_access` */

DROP TABLE IF EXISTS `admin_access`;

CREATE TABLE `admin_access` (
  `adacAdmnId` int(10) unsigned NOT NULL DEFAULT '0',
  `adacMenuId` int(10) unsigned NOT NULL DEFAULT '0',
  `adacView` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `adacNew` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `adacEdit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `adacDelete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `adacConfirm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `adacVoid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`adacAdmnId`,`adacMenuId`),
  KEY `adacMenuId` (`adacMenuId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Daftar Akses User Admin';

/*Data for the table `admin_access` */

insert  into `admin_access`(`adacAdmnId`,`adacMenuId`,`adacView`,`adacNew`,`adacEdit`,`adacDelete`,`adacConfirm`,`adacVoid`) values 
(1,67,1,1,1,1,1,1),
(1,68,1,1,1,1,1,1),
(1,71,1,1,1,1,1,1),
(1,73,1,1,1,1,1,1),
(1,74,1,1,1,1,1,1),
(1,75,1,1,1,1,1,1),
(1,76,1,1,1,1,1,0),
(1,77,1,1,1,1,1,0),
(1,78,1,1,1,1,1,0),
(1,79,1,1,1,1,1,1),
(1,80,1,1,1,1,1,1),
(1,81,1,1,1,1,1,1),
(1,83,1,1,1,1,1,1),
(1,84,1,1,1,1,1,1),
(1,85,1,1,1,1,1,1),
(1,86,1,1,1,1,1,1),
(1,87,1,1,1,1,1,1),
(1,88,1,1,1,1,1,0),
(2,53,1,1,1,1,1,1),
(2,71,1,1,1,1,1,1),
(2,76,1,1,1,1,1,0),
(2,77,1,1,1,1,1,0),
(2,78,1,1,1,1,1,0),
(2,88,1,1,1,1,1,0),
(4,3,1,1,1,1,1,1),
(4,4,1,1,1,1,1,1),
(4,6,1,1,1,1,1,1),
(4,7,1,1,1,1,1,1),
(4,31,1,1,1,1,1,1),
(4,33,1,1,1,1,1,0),
(4,34,1,1,1,1,1,0),
(4,35,1,1,1,1,1,1),
(4,36,1,1,1,1,1,1),
(4,37,1,1,1,1,1,1),
(4,38,1,1,1,1,1,1),
(4,39,1,1,1,1,1,1),
(4,40,1,1,1,1,1,1),
(4,41,1,1,1,1,1,1),
(4,42,1,1,1,1,1,1),
(4,43,1,1,1,1,1,1),
(4,44,1,1,1,1,1,1),
(4,45,1,1,1,1,1,1),
(4,46,1,1,1,1,1,1),
(4,47,1,1,1,1,1,1),
(4,48,1,1,1,1,1,1),
(4,49,1,1,1,1,1,1),
(4,50,1,1,1,1,1,1),
(4,51,1,1,1,1,1,1),
(4,52,1,1,1,1,1,1),
(4,53,1,1,1,1,1,1),
(4,61,1,1,1,1,1,1),
(4,62,1,1,1,1,1,1),
(4,63,1,1,1,1,1,1),
(4,64,1,1,1,1,1,1),
(4,65,1,1,1,1,1,1),
(4,66,1,1,1,1,0,0),
(4,67,1,1,1,1,1,1),
(4,68,1,1,1,1,1,1),
(4,71,1,1,1,1,1,1),
(4,73,1,1,1,1,1,1),
(4,74,1,1,1,1,1,1),
(4,75,1,1,1,1,1,1),
(4,76,1,1,1,1,1,0),
(4,77,1,1,1,1,1,0),
(4,78,1,1,1,1,1,0),
(4,79,1,1,1,1,1,1),
(4,80,1,1,1,1,1,1),
(4,81,1,1,1,1,1,1),
(4,83,1,1,1,1,1,1),
(4,84,1,1,1,1,1,1),
(4,85,1,1,1,1,1,1),
(4,86,1,1,1,1,1,1),
(4,87,1,1,1,1,1,1),
(4,88,1,1,1,1,1,1),
(5,1,1,1,1,1,1,1),
(5,2,1,1,1,1,0,0),
(5,3,1,1,1,1,1,0),
(5,4,1,1,1,1,1,0),
(5,6,1,1,1,1,1,0),
(5,7,1,1,1,1,1,1),
(5,8,1,1,1,1,0,0),
(5,9,1,1,1,1,0,0),
(5,10,1,1,1,1,0,0),
(5,11,1,1,1,1,0,0),
(5,12,1,1,1,1,0,0),
(5,13,1,1,1,1,0,0),
(5,14,1,1,1,1,0,0),
(5,15,1,1,1,1,0,0),
(5,16,1,1,1,1,0,0),
(5,17,1,1,1,1,0,0),
(5,18,1,1,1,1,0,0),
(5,21,1,1,1,1,0,0),
(5,22,1,1,1,1,0,0),
(5,23,1,1,1,1,0,0),
(5,24,1,1,1,1,0,0),
(5,25,1,1,1,1,0,0),
(5,26,1,1,1,1,0,0),
(5,27,1,1,1,1,0,0),
(5,28,1,1,1,1,0,0),
(5,29,1,1,1,1,0,0),
(5,30,1,1,1,1,0,0),
(5,31,1,1,1,1,1,1),
(5,32,1,1,1,1,1,0),
(5,33,1,1,1,1,0,0),
(5,34,1,1,1,1,0,0),
(5,35,1,1,1,1,1,1),
(5,36,1,1,1,1,1,1),
(5,37,1,1,1,1,1,1),
(5,38,1,1,1,1,1,1),
(5,39,1,1,1,1,1,1),
(5,40,1,1,1,1,1,1),
(5,41,1,1,1,1,1,1),
(5,42,1,1,1,1,1,0),
(5,43,1,1,1,1,1,1),
(5,44,1,1,1,1,1,1),
(5,45,1,1,1,1,1,1),
(5,46,1,1,1,1,1,1),
(5,47,1,1,1,1,1,1),
(5,48,1,1,1,1,1,1),
(5,49,1,1,1,1,1,1),
(5,50,1,1,1,1,1,1),
(5,51,1,1,1,1,1,1),
(5,52,1,1,1,1,1,1),
(5,53,1,1,1,1,1,1),
(5,61,1,1,1,1,1,1),
(5,62,1,1,1,1,1,1),
(5,63,1,1,1,1,1,1),
(5,64,1,1,1,1,1,1),
(5,65,1,1,1,1,1,1),
(5,66,1,1,1,1,0,0),
(5,67,1,1,1,1,1,1),
(5,68,1,1,1,1,1,1),
(5,71,1,1,1,1,1,1),
(5,73,1,1,1,1,1,1),
(5,74,1,1,1,1,1,1),
(5,75,1,1,1,1,1,1),
(5,76,1,1,1,1,1,0),
(5,77,1,1,1,1,1,0),
(5,78,1,1,1,1,1,0),
(5,79,1,1,1,1,1,1),
(5,80,1,1,1,1,1,1),
(5,81,1,1,1,1,1,1),
(5,83,1,1,1,1,1,1),
(5,84,1,1,1,1,1,1),
(5,85,1,1,1,1,1,1),
(5,86,1,1,1,1,1,1),
(5,87,1,1,1,1,1,1),
(5,88,1,1,1,1,1,0),
(7,65,1,1,1,1,1,1),
(7,66,1,1,1,1,0,0),
(7,67,1,1,1,1,1,1),
(7,68,1,1,1,1,1,1),
(7,71,1,1,1,1,1,1),
(7,73,1,1,1,1,1,1),
(7,74,1,1,1,1,1,1),
(7,75,1,1,1,1,1,1),
(7,76,1,1,1,1,1,0),
(7,77,1,1,1,1,1,0),
(7,78,1,1,1,1,1,0),
(7,79,1,1,1,1,1,1),
(7,80,1,1,1,1,1,1),
(7,81,1,1,1,1,1,1),
(7,83,1,1,1,1,1,1),
(7,84,1,1,1,1,1,1),
(7,85,1,1,1,1,1,1),
(7,86,1,1,1,1,1,1),
(7,87,1,1,1,1,1,1),
(7,88,1,1,1,1,1,0),
(8,1,1,1,1,1,1,1),
(8,2,1,1,1,1,0,0),
(8,3,1,1,1,1,1,0),
(8,4,1,1,1,1,1,0),
(8,6,1,1,1,1,1,0),
(8,7,1,1,1,1,1,1),
(8,8,1,1,1,1,0,0),
(8,9,1,1,1,1,0,0),
(8,10,1,1,1,1,0,0),
(8,11,1,1,1,1,0,0),
(8,12,1,1,1,1,0,0),
(8,13,1,1,1,1,0,0),
(8,14,1,1,1,1,0,0),
(8,15,1,1,1,1,0,0),
(8,16,1,1,1,1,0,0),
(8,17,1,1,1,1,0,0),
(8,18,1,1,1,1,0,0),
(8,21,1,1,1,1,0,0),
(8,22,1,1,1,1,0,0),
(8,23,1,1,1,1,0,0),
(8,24,1,1,1,1,0,0),
(8,25,1,1,1,1,0,0),
(8,26,1,1,1,1,0,0),
(8,27,1,1,1,1,0,0),
(8,28,1,1,1,1,0,0),
(8,29,1,1,1,1,0,0),
(8,30,1,1,1,1,0,0),
(8,31,1,1,1,1,1,1),
(8,32,1,1,1,1,1,0),
(8,33,1,1,1,1,0,0),
(8,34,1,1,1,1,0,0),
(8,35,1,1,1,1,1,1),
(8,36,1,1,1,1,1,1),
(8,37,1,1,1,1,1,1),
(8,38,1,1,1,1,1,1),
(8,39,1,1,1,1,1,1),
(8,40,1,1,1,1,1,1),
(8,41,1,1,1,1,1,1),
(8,42,1,1,1,1,1,0),
(8,43,1,1,1,1,1,1),
(8,44,1,1,1,1,1,1),
(8,45,1,1,1,1,1,1),
(8,46,1,1,1,1,1,1),
(8,47,1,1,1,1,1,1),
(8,48,1,1,1,1,1,1),
(8,49,1,1,1,1,1,1),
(8,50,1,1,1,1,1,1),
(8,51,1,1,1,1,1,1),
(8,52,1,1,1,1,1,1),
(8,53,1,1,1,1,1,1),
(8,61,1,1,1,1,1,1),
(8,62,1,1,1,1,1,1),
(8,63,1,1,1,1,1,1),
(8,64,1,1,1,1,1,1),
(8,65,1,1,1,1,1,1),
(8,66,1,1,1,1,0,0),
(8,67,1,1,1,1,1,1),
(8,68,1,1,1,1,1,1),
(8,71,1,1,1,1,1,1),
(8,73,1,1,1,1,1,1),
(8,74,1,1,1,1,1,1),
(8,75,1,1,1,1,1,1),
(8,76,1,1,1,1,1,0),
(8,77,1,1,1,1,1,0),
(8,78,1,1,1,1,1,0),
(8,79,1,1,1,1,1,1),
(8,80,1,1,1,1,1,1),
(8,81,1,1,1,1,1,1),
(8,82,1,1,1,1,1,1),
(8,83,1,1,1,1,1,1),
(8,84,1,1,1,1,1,1),
(8,85,1,1,1,1,1,1),
(8,86,1,1,1,1,1,1),
(8,87,1,1,1,1,1,1),
(8,88,1,1,1,1,1,0),
(9,1,1,1,1,1,1,1),
(9,2,1,1,1,1,0,0),
(9,3,1,1,1,1,1,0),
(9,4,1,1,1,1,1,0),
(9,6,1,1,1,1,1,0),
(9,7,1,1,1,1,1,1),
(9,8,1,1,1,1,0,0),
(9,9,1,1,1,1,0,0),
(9,10,1,1,1,1,0,0),
(9,11,1,1,1,1,0,0),
(9,12,1,1,1,1,0,0),
(9,13,1,1,1,1,0,0),
(9,14,1,1,1,1,0,0),
(9,15,1,1,1,1,0,0),
(9,16,1,1,1,1,0,0),
(9,17,1,1,1,1,0,0),
(9,18,1,1,1,1,0,0),
(9,21,1,1,1,1,0,0),
(9,22,1,1,1,1,0,0),
(9,23,1,1,1,1,0,0),
(9,24,1,1,1,1,0,0),
(9,25,1,1,1,1,0,0),
(9,26,1,1,1,1,0,0),
(9,27,1,1,1,1,0,0),
(9,28,1,1,1,1,0,0),
(9,29,1,1,1,1,0,0),
(9,30,1,1,1,1,0,0),
(9,31,1,1,1,1,1,1),
(9,32,1,1,1,1,1,0),
(9,33,1,1,1,1,0,0),
(9,34,1,1,1,1,0,0),
(9,35,1,1,1,1,1,1),
(9,36,1,1,1,1,1,1),
(9,37,1,1,1,1,1,1),
(9,38,1,1,1,1,1,1),
(9,39,1,1,1,1,1,1),
(9,40,1,1,1,1,1,1),
(9,41,1,1,1,1,1,1),
(9,42,1,1,1,1,1,0),
(9,43,1,1,1,1,1,1),
(9,44,1,1,1,1,1,1),
(9,45,1,1,1,1,1,1),
(9,46,1,1,1,1,1,1),
(9,47,1,1,1,1,1,1),
(9,48,1,1,1,1,1,1),
(9,49,1,1,1,1,1,1),
(9,50,1,1,1,1,1,1),
(9,51,1,1,1,1,1,1),
(9,52,1,1,1,1,1,1),
(9,53,1,1,1,1,1,1),
(9,61,1,1,1,1,1,1),
(9,62,1,1,1,1,1,1),
(9,63,1,1,1,1,1,1),
(9,64,1,1,1,1,1,1),
(9,65,1,1,1,1,1,1),
(9,66,1,1,1,1,0,0),
(9,67,1,1,1,1,1,1),
(9,68,1,1,1,1,1,1),
(9,71,1,1,1,1,1,1),
(9,73,1,1,1,1,1,1),
(9,74,1,1,1,1,1,1),
(9,75,1,1,1,1,1,1),
(9,76,1,1,1,1,1,0),
(9,77,1,1,1,1,1,0),
(9,78,1,1,1,1,1,0),
(9,79,1,1,1,1,1,1),
(9,80,1,1,1,1,1,1),
(9,81,1,1,1,1,1,1),
(9,82,1,1,1,1,1,1),
(9,83,1,1,1,1,1,1),
(9,84,1,1,1,1,1,1),
(9,85,1,1,1,1,1,1),
(9,86,1,1,1,1,1,1),
(9,87,1,1,1,1,1,1),
(9,88,1,1,1,1,1,0),
(10,1,1,1,1,1,1,1),
(10,2,1,1,1,1,0,0),
(10,3,1,1,1,1,1,0),
(10,4,1,1,1,1,1,0),
(10,6,1,1,1,1,1,0),
(10,7,1,1,1,1,1,1),
(10,8,1,1,1,1,0,0),
(10,9,1,1,1,1,0,0),
(10,10,1,1,1,1,0,0),
(10,11,1,1,1,1,0,0),
(10,12,1,1,1,1,0,0),
(10,13,1,1,1,1,0,0),
(10,14,1,1,1,1,0,0),
(10,15,1,1,1,1,0,0),
(10,16,1,1,1,1,0,0),
(10,17,1,1,1,1,0,0),
(10,18,1,1,1,1,0,0),
(10,21,1,1,1,1,0,0),
(10,22,1,1,1,1,0,0),
(10,23,1,1,1,1,0,0),
(10,24,1,1,1,1,0,0),
(10,25,1,1,1,1,0,0),
(10,26,1,1,1,1,0,0),
(10,27,1,1,1,1,0,0),
(10,28,1,1,1,1,0,0),
(10,29,1,1,1,1,0,0),
(10,30,1,1,1,1,0,0),
(10,31,1,1,1,1,1,1),
(10,32,1,1,1,1,1,0),
(10,33,1,1,1,1,0,0),
(10,34,1,1,1,1,0,0),
(10,35,1,1,1,1,1,1),
(10,36,1,1,1,1,1,1),
(10,37,1,1,1,1,1,1),
(10,38,1,1,1,1,1,1),
(10,39,1,1,1,1,1,1),
(10,40,1,1,1,1,1,1),
(10,41,1,1,1,1,1,1),
(10,42,1,1,1,1,1,0),
(10,43,1,1,1,1,1,1),
(10,44,1,1,1,1,1,1),
(10,45,1,1,1,1,1,1),
(10,46,1,1,1,1,1,1),
(10,47,1,1,1,1,1,1),
(10,48,1,1,1,1,1,1),
(10,49,1,1,1,1,1,1),
(10,50,1,1,1,1,1,1),
(10,51,1,1,1,1,1,1),
(10,52,1,1,1,1,1,1),
(10,53,1,1,1,1,1,1),
(10,61,1,1,1,1,1,1),
(10,62,1,1,1,1,1,1),
(10,63,1,1,1,1,1,1),
(10,64,1,1,1,1,1,1),
(10,65,1,1,1,1,1,1),
(10,66,1,1,1,1,0,0),
(10,67,1,1,1,1,1,1),
(10,68,1,1,1,1,1,1),
(10,71,1,1,1,1,1,1),
(10,73,1,1,1,1,1,1),
(10,74,1,1,1,1,1,1),
(10,75,1,1,1,1,1,1),
(10,76,1,1,1,1,1,0),
(10,77,1,1,1,1,1,0),
(10,78,1,1,1,1,1,0),
(10,79,1,1,1,1,1,1),
(10,80,1,1,1,1,1,1),
(10,81,1,1,1,1,1,1),
(10,82,1,1,1,1,1,1),
(10,83,1,1,1,1,1,1),
(10,84,1,1,1,1,1,1),
(10,85,1,1,1,1,1,1),
(10,86,1,1,1,1,1,1),
(10,87,1,1,1,1,1,1),
(10,88,1,1,1,1,1,0);

/*Table structure for table `admin_company` */

DROP TABLE IF EXISTS `admin_company`;

CREATE TABLE `admin_company` (
  `adcoAdmnId` int(10) unsigned NOT NULL DEFAULT '0',
  `adcoCompId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`adcoAdmnId`,`adcoCompId`),
  KEY `adcoCompId` (`adcoCompId`),
  CONSTRAINT `admin_company_ibfk_1` FOREIGN KEY (`adcoAdmnId`) REFERENCES `admin_users` (`ausrId`),
  CONSTRAINT `admin_company_ibfk_2` FOREIGN KEY (`adcoCompId`) REFERENCES `company` (`compId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Daftar Akses User Admin ke Company';

/*Data for the table `admin_company` */

/*Table structure for table `admin_logs` */

DROP TABLE IF EXISTS `admin_logs`;

CREATE TABLE `admin_logs` (
  `alogAdminId` int(10) unsigned DEFAULT NULL,
  `alogAdminLogin` varchar(50) NOT NULL,
  `alogTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `alogIP` varchar(100) NOT NULL,
  `alogStatus` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '0:Success; 1:Failed; 2:Banned; 3:Doubled',
  KEY `admin` (`alogAdminId`,`alogTime`),
  KEY `ip` (`alogIP`,`alogTime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Admin Log';

/*Data for the table `admin_logs` */

/*Table structure for table `admin_sessions` */

DROP TABLE IF EXISTS `admin_sessions`;

CREATE TABLE `admin_sessions` (
  `asesAdminId` int(10) unsigned NOT NULL DEFAULT '0',
  `asesSessionId` varchar(50) NOT NULL,
  `asesModule` varchar(255) NOT NULL,
  `asesLastAccess` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`asesAdminId`),
  UNIQUE KEY `asesSessionId` (`asesSessionId`),
  KEY `asesModule` (`asesModule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Daftar Session User';

/*Data for the table `admin_sessions` */

/*Table structure for table `admin_user_sessions` */

DROP TABLE IF EXISTS `admin_user_sessions`;

CREATE TABLE `admin_user_sessions` (
  `usesUserId` int(10) unsigned NOT NULL,
  `usesSessionId` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `usesComp` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `usesLastAccess` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`usesUserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Daftar User Session';

/*Data for the table `admin_user_sessions` */

insert  into `admin_user_sessions`(`usesUserId`,`usesSessionId`,`usesComp`,`usesLastAccess`) values 
(2,'cd3f410cba55290108038cbc02192bda93987fa0','e6cb5b74698de7b11e95e44c4ccb32b4','2015-02-12 13:09:14');

/*Table structure for table `admin_users` */

DROP TABLE IF EXISTS `admin_users`;

CREATE TABLE `admin_users` (
  `ausrId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ausrUsername` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `ausrPassword` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `ausrName` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `ausrActive` tinyint(1) NOT NULL DEFAULT '0',
  `ausrLastLogin` datetime NOT NULL,
  `ausrCreated` datetime NOT NULL,
  `ausrRolhId` int(10) NOT NULL DEFAULT '0',
  `ausrUnit` int(10) NOT NULL DEFAULT '0',
  `ausrFirstLogin` tinyint(4) NOT NULL DEFAULT '0',
  `ausrBannedTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ausrId`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Daftar Table User';

/*Data for the table `admin_users` */

insert  into `admin_users`(`ausrId`,`ausrUsername`,`ausrPassword`,`ausrName`,`ausrActive`,`ausrLastLogin`,`ausrCreated`,`ausrRolhId`,`ausrUnit`,`ausrFirstLogin`,`ausrBannedTime`) values 
(30,'root','12345','root',0,'2018-08-27 14:05:56','0000-00-00 00:00:00',1,1,0,'0000-00-00 00:00:00'),
(59,'Odi','$2y$10$WQiorUwBnBbZE5zUZuTyYO/aAx8DOIENja56y7.jCVISYrSLMd9/q','Odi Nugraha',0,'2019-07-01 11:38:26','0000-00-00 00:00:00',1,0,0,'0000-00-00 00:00:00');

/*Table structure for table `admin_users_access` */

DROP TABLE IF EXISTS `admin_users_access`;

CREATE TABLE `admin_users_access` (
  `auacAusrId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ID Admin User',
  `auacMenuId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ID Menu',
  `auacView` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `auacNew` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `auacEdit` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `auacDelete` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`auacAusrId`,`auacMenuId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Daftar Akses User Admin';

/*Data for the table `admin_users_access` */

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `compId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `compNick` varchar(10) NOT NULL,
  `compName` varchar(35) NOT NULL,
  `compAddress` varchar(75) NOT NULL,
  `compPostCode` varchar(5) NOT NULL,
  `compCity` varchar(50) NOT NULL,
  `compTelp` varchar(15) NOT NULL,
  `compTelp2` varchar(15) NOT NULL,
  `compFax` varchar(15) NOT NULL,
  `compFax2` varchar(15) NOT NULL,
  `compEmail` varchar(25) NOT NULL,
  `compNonActiveFlag` tinyint(3) NOT NULL DEFAULT '0' COMMENT '0: Active | 1: Non Active',
  `compStatusAnggaran` tinyint(1) NOT NULL DEFAULT '1',
  `comWorkingDate` date NOT NULL DEFAULT '0000-00-00',
  `compCreatedTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `compCreatedUserId` int(11) NOT NULL,
  `compUpdatedTime` datetime DEFAULT NULL,
  `compUpdatedUserId` int(11) DEFAULT NULL,
  `compDeletedTime` datetime DEFAULT NULL,
  `compDeletedUserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`compId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Daftar Company';

/*Data for the table `company` */

insert  into `company`(`compId`,`compNick`,`compName`,`compAddress`,`compPostCode`,`compCity`,`compTelp`,`compTelp2`,`compFax`,`compFax2`,`compEmail`,`compNonActiveFlag`,`compStatusAnggaran`,`comWorkingDate`,`compCreatedTime`,`compCreatedUserId`,`compUpdatedTime`,`compUpdatedUserId`,`compDeletedTime`,`compDeletedUserId`) values 
(1,'Samawa','Kabupaten Sumbawa','tower','12345','Gotham City','2222222','33333333','44444444','555555555','sss@sss.com',0,1,'2015-03-03','2014-12-10 00:00:00',2,'2015-02-23 11:57:15',0,NULL,NULL);

/*Table structure for table `doc_sch` */

DROP TABLE IF EXISTS `doc_sch`;

CREATE TABLE `doc_sch` (
  `doc_schId` int(11) NOT NULL AUTO_INCREMENT,
  `doc_schNumb` varchar(100) DEFAULT NULL,
  `doc_schName` varchar(50) DEFAULT NULL,
  `doc_schDay` varchar(10) DEFAULT NULL,
  `doc_schPeriodStart` time DEFAULT NULL,
  `doc_schPeriodStop` time DEFAULT NULL,
  `doc_schPoly` int(11) DEFAULT NULL,
  `doc_schCreateTime` datetime DEFAULT NULL,
  `doc_schCreateUser` varchar(225) DEFAULT NULL,
  `doc_schUpdateTime` datetime DEFAULT NULL,
  `doc_schUpdateUser` varchar(225) DEFAULT NULL,
  `doc_schDeleteTime` datetime DEFAULT NULL,
  `doc_schDeleteUser` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`doc_schId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `doc_sch` */

insert  into `doc_sch`(`doc_schId`,`doc_schNumb`,`doc_schName`,`doc_schDay`,`doc_schPeriodStart`,`doc_schPeriodStop`,`doc_schPoly`,`doc_schCreateTime`,`doc_schCreateUser`,`doc_schUpdateTime`,`doc_schUpdateUser`,`doc_schDeleteTime`,`doc_schDeleteUser`) values 
(1,'0189U003','BAMBANG WITARNO, dr','Seni','00:00:08','00:00:09',NULL,'2019-07-01 12:39:37','Odi',NULL,NULL,NULL,NULL),
(2,'0189U003','BAMBANG WITARNO, dr','Seni','08:00:00','12:00:00',1,'2019-07-01 12:44:22','Odi',NULL,NULL,NULL,NULL),
(3,'0189U008','TITIS SULISTYOWATI, dr','Seni','08:00:00','12:00:00',3,'2019-07-01 12:46:27','Odi',NULL,NULL,NULL,NULL),
(4,'0189U009','WISNU WIDODO EKA PUTRA, dr','Senin','08:00:00','12:00:00',3,'2019-07-01 12:47:20','Odi',NULL,NULL,NULL,NULL),
(5,'0189U010','ADI WIDJAJA, dr','Senin','08:00:00','12:00:00',3,'2019-07-01 12:47:52','Odi',NULL,NULL,NULL,NULL),
(6,'0189U012','RITA WAHYUNINGSIH, dr','Senin','08:00:00','12:00:00',3,'2019-07-01 12:48:38','Odi',NULL,NULL,NULL,NULL);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `menuId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuScope` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1: admin; 2: user; 3: public',
  `menuName` varchar(50) NOT NULL,
  `menuNameInd` varchar(50) DEFAULT NULL,
  `menuNameEng` varchar(50) DEFAULT NULL,
  `apiLangGrid` varchar(255) DEFAULT NULL,
  `apiLangForm` varchar(255) DEFAULT NULL,
  `apiData` varchar(255) DEFAULT NULL,
  `menuLink` varchar(100) DEFAULT NULL COMMENT 'Jika modal=1, maka link datanya null',
  `htmlLink` varchar(200) DEFAULT '#',
  `menuModal` tinyint(4) DEFAULT '0',
  `menuParentId` int(10) DEFAULT '0',
  `menuIcon` varchar(100) DEFAULT NULL,
  `menuOrder` tinyint(4) DEFAULT '0',
  `menuLevel` tinyint(3) DEFAULT '0',
  `menuHeader` tinyint(3) DEFAULT '0' COMMENT '1: Header; 0: Detail',
  `menuFormCode` int(11) DEFAULT NULL,
  `menuNonActive` tinyint(4) DEFAULT '0',
  `menuCreatedTime` datetime DEFAULT '0000-00-00 00:00:00',
  `menuCreatedUserId` int(11) DEFAULT '2',
  `menuUpdatedTime` datetime DEFAULT NULL,
  `menuUpdatedUserId` int(11) DEFAULT NULL,
  `menuDeletedTime` datetime DEFAULT NULL,
  `menuDeletedUserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`menuId`),
  KEY `menuParentId` (`menuParentId`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 COMMENT='Daftar Menu';

/*Data for the table `menu` */

insert  into `menu`(`menuId`,`menuScope`,`menuName`,`menuNameInd`,`menuNameEng`,`apiLangGrid`,`apiLangForm`,`apiData`,`menuLink`,`htmlLink`,`menuModal`,`menuParentId`,`menuIcon`,`menuOrder`,`menuLevel`,`menuHeader`,`menuFormCode`,`menuNonActive`,`menuCreatedTime`,`menuCreatedUserId`,`menuUpdatedTime`,`menuUpdatedUserId`,`menuDeletedTime`,`menuDeletedUserId`) values 
(2,1,'Master ','','','','','','','#',0,0,'fa-table',3,0,0,0,0,'2015-02-17 09:47:43',2,'2017-09-13 15:13:07',0,NULL,NULL),
(3,1,'User','','','backend/public/lang/admin/config/useradmin/grid','backend/public/lang/admin/config/useradmin/form','backend/public/api/admin/config/useradmin','user','ajax/master/master.html',0,7,'',1,0,0,NULL,0,'0001-01-01 00:00:00',2,'2018-06-06 00:53:08',0,NULL,NULL),
(7,1,'Settings','','','','','','','#',0,0,'fa-gears',1,0,0,NULL,0,'0001-01-01 00:00:00',2,'2015-11-26 15:11:44',0,NULL,NULL),
(8,1,'Menu','','','backend/public/lang/admin/config/useradminmenu/grid','backend/public/lang/admin/config/useradminmenu/form','backend/public/api/admin/config/menuadminapi','Menu','ajax/master/master.html',0,7,'',2,0,0,NULL,0,'0001-01-01 00:00:00',2,'2015-09-15 20:34:00',0,NULL,NULL),
(14,1,'Grup','','','backend/public/lang/admin/config/role/grid','backend/public/lang/admin/config/role/form','backend/public/api/admin/config/role','Grup','ajax/master/master.html',0,7,'',3,0,0,NULL,0,'2015-04-27 11:13:21',0,'2015-11-26 15:16:27',0,NULL,NULL),
(15,1,'Grup Menu','','','backend/public/lang/admin/config/role/grid','backend/public/lang/admin/config/role/form','backend/public/api/admin/config/role','Grupmenu','ajax/setup/rolemenu.html',0,7,'',4,0,0,NULL,0,'2015-04-27 11:26:33',0,'2015-11-26 15:16:59',0,NULL,NULL),
(41,1,'Konfigurasi',NULL,NULL,'backend/public/lang/admin/transaksi/homepage/grid','backend/public/lang/admin/transaksi/homepage/form','backend/public/api/admin/transaksi/homepage','Client','ajax/master/master2.html',0,2,'',20,0,0,NULL,0,'2015-09-15 20:39:40',0,'2015-10-14 12:43:36',0,NULL,NULL),
(154,1,'Pelayanan Medis',NULL,NULL,'backend/public/lang/admin/master/hospital/grid','backend/public/lang/admin/master/hospital/form','backend/public/api/admin/master/hospital','Pelayanan Medis','ajax/master/masterhospital.html',0,2,'',1,0,0,NULL,0,'2018-02-15 13:39:32',0,'2019-06-24 20:31:56',0,NULL,NULL),
(162,1,'Poli',NULL,NULL,'backend/public/lang/admin/master/poly/grid','backend/public/lang/admin/master/poly/form','backend/public/api/admin/master/poli','Poli','ajax/master/masterpoli.html',0,2,'',2,0,0,NULL,0,'2019-06-23 21:04:27',0,'2019-06-24 20:18:29',0,NULL,NULL),
(164,1,'Pendaftaran Online',NULL,NULL,'','','','Pendaftaran Online','#',0,0,'fa-hospital-o',5,0,0,NULL,0,'2019-06-24 20:27:52',0,'2019-07-01 11:40:18',0,NULL,NULL),
(165,1,'Calling Dokter',NULL,NULL,'','','','Calling Dokter','#',0,0,'fa-user-md',6,0,0,NULL,0,'2019-06-24 20:29:28',0,'2019-07-01 11:40:29',0,NULL,NULL),
(166,1,'Daftar',NULL,NULL,'','','','Daftar','ajax/user/daftaronline.html',0,164,'',1,0,0,NULL,0,'2019-06-24 21:01:19',0,'2019-06-24 21:04:34',0,NULL,NULL),
(167,1,'Pengaturan Pelayanan',NULL,NULL,'','','','Pengaturan Pelayanan','#',0,0,'fa-cog',4,0,0,NULL,0,'2019-07-01 11:40:12',0,'2019-07-01 11:47:47',0,NULL,NULL),
(168,1,'Jadwal Dokter',NULL,NULL,'backend/public/lang/admin/master/schedule/grid','backend/public/lang/admin/master/schedule/form','backend/public/api/admin/master/schedule','Jadwal Dokter','ajax/master/master.html',0,167,'',1,0,0,NULL,0,'2019-07-01 11:43:23',0,'2019-07-01 12:24:31',0,NULL,NULL);

/*Table structure for table `mshospital` */

DROP TABLE IF EXISTS `mshospital`;

CREATE TABLE `mshospital` (
  `mshospitalId` int(11) NOT NULL AUTO_INCREMENT,
  `mshospitalName` varchar(100) DEFAULT NULL,
  `mshospitalAddress` varchar(100) DEFAULT NULL,
  `mshospitalLoc` varchar(100) DEFAULT NULL,
  `mshospitalCreateTime` datetime DEFAULT NULL,
  `mshospitalCreateUser` varchar(100) DEFAULT NULL,
  `mshospitalUpdateTime` datetime DEFAULT NULL,
  `mshospitalUpdateUser` varchar(100) DEFAULT NULL,
  `mshospitalDeleteTime` datetime DEFAULT NULL,
  `mshospitalDeleteUser` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`mshospitalId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `mshospital` */

insert  into `mshospital`(`mshospitalId`,`mshospitalName`,`mshospitalAddress`,`mshospitalLoc`,`mshospitalCreateTime`,`mshospitalCreateUser`,`mshospitalUpdateTime`,`mshospitalUpdateUser`,`mshospitalDeleteTime`,`mshospitalDeleteUser`) values 
(1,'RS Baladhika Husada','Jalan Panglima Besar Sudirman No.45, Jemberlor, Patrang, Pagah, Jemberlor, Patrang, Kabupaten Jember','12321323','2019-06-23 20:49:59','Odi','2019-06-23 21:33:55','Odi',NULL,NULL),
(2,'RS Citra Husada','Jl. Teratai No.22, Gebang Timur, Gebang, Patrang, Kabupaten Jember, Jawa Timur 68117','123213213','2019-06-23 20:50:18','Odi','2019-06-23 21:34:02','Odi',NULL,NULL),
(3,'RS Paru Jember','Jl. Nusa Indah No.28, Krajan, Jemberlor, Patrang, Kabupaten Jember, Jawa Timur 68118','12321321313','2019-06-23 20:50:40','Odi','2019-06-23 21:34:10','Odi',NULL,NULL),
(4,'RSU Bina Sehat','Jl. Jayanegara No.7, Kaliwates Kidul, Kaliwates, Kabupaten Jember, Jawa Timur 68131','123213123','2019-06-23 20:50:59','Odi','2019-06-23 21:34:18','Odi',NULL,NULL),
(5,'RSU Jember Klinik','Jl. Bedadung No.2, Kp. Using, Jemberlor, Patrang, Kabupaten Jember, Jawa Timur 68118','123213213123','2019-06-23 20:51:20','Odi','2019-06-23 21:34:28','Odi',NULL,NULL),
(6,'RSU Kaliwates','Jl. Diah Pitaloka No.4A, Kaliwates Kidul, Kaliwates, Kabupaten Jember, Jawa Timur 68131','312312321','2019-06-23 20:51:40','Odi','2019-06-23 21:34:36','Odi',NULL,NULL),
(7,'RSUD Dr. Soebandi','Jl. DR. Soebandi No.124, Cangkring, Patrang, Kabupaten Jember, Jawa Timur 68111','313123213','2019-06-23 20:52:06','Odi','2019-06-23 21:34:45','Odi',NULL,NULL),
(8,'Rumah Sakit Umum TNI Angkatan Darat (DKT)','Jl. PB Sudirman No.45, Pagah, Jemberlor, Patrang, Kabupaten Jember, Jawa Timur 68118','123213123213','2019-06-23 20:52:26','Odi','2019-06-23 21:34:57','Odi',NULL,NULL);

/*Table structure for table `mspoly` */

DROP TABLE IF EXISTS `mspoly`;

CREATE TABLE `mspoly` (
  `mspolyId` int(11) NOT NULL AUTO_INCREMENT,
  `mspolyHospital` int(11) DEFAULT NULL,
  `mspolyName` varchar(50) DEFAULT NULL,
  `mspolyCreateTime` datetime DEFAULT NULL,
  `mspolyCreateUser` varchar(100) DEFAULT NULL,
  `mspolyUpdateTime` datetime DEFAULT NULL,
  `mspolyUpdateUser` varchar(100) DEFAULT NULL,
  `mspolyDeleteTime` datetime DEFAULT NULL,
  `mspolyDeleteUser` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`mspolyId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mspoly` */

insert  into `mspoly`(`mspolyId`,`mspolyHospital`,`mspolyName`,`mspolyCreateTime`,`mspolyCreateUser`,`mspolyUpdateTime`,`mspolyUpdateUser`,`mspolyDeleteTime`,`mspolyDeleteUser`) values 
(1,2,'Poli Dalam','2019-06-23 21:35:13','Odi','2019-06-23 21:36:20','Odi',NULL,NULL),
(2,8,'Poli Anak','2019-06-23 21:59:49','Odi',NULL,NULL,NULL,NULL),
(3,4,'Poli Umum','2019-07-01 12:45:14','Odi',NULL,NULL,NULL,NULL);

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `roleId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roleName` varchar(50) NOT NULL,
  `roleNonActive` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `roleCreatedUserId` varchar(25) DEFAULT NULL,
  `roleCreatedTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `roleUpdatedUserId` varchar(25) DEFAULT NULL,
  `roleUpdatedTime` datetime DEFAULT NULL,
  `roleDeletedUserId` varchar(25) DEFAULT NULL,
  `roleDeletedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Daftar Role';

/*Data for the table `role` */

insert  into `role`(`roleId`,`roleName`,`roleNonActive`,`roleCreatedUserId`,`roleCreatedTime`,`roleUpdatedUserId`,`roleUpdatedTime`,`roleDeletedUserId`,`roleDeletedTime`) values 
(1,'Administrator',0,'2','2014-12-02 06:47:29','2','2014-12-17 10:46:10',NULL,NULL),
(2,'Dokter',0,'Tumbar','2018-06-05 04:18:09','Odi','2019-06-23 20:37:44',NULL,NULL),
(3,'Pasien',0,'Tumbar','2018-06-05 04:18:23','Odi','2019-06-23 20:37:51',NULL,NULL),
(8,'Rumah Sakit',0,'Odi','2019-06-23 20:38:25',NULL,NULL,NULL,NULL);

/*Table structure for table `role_menu` */

DROP TABLE IF EXISTS `role_menu`;

CREATE TABLE `role_menu` (
  `rolmId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rolmRoleId` int(10) unsigned DEFAULT NULL,
  `rolmMenuId` int(10) unsigned DEFAULT NULL,
  `rolmView` tinyint(3) NOT NULL DEFAULT '0',
  `rolmNew` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rolmEdit` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rolmDelete` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rolmConfirm` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rolmApprove` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rolmVoid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rolmCreatedUserId` varchar(25) DEFAULT NULL,
  `rolmCreatedTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rolmUpdatedUserId` varchar(25) DEFAULT NULL,
  `rolmUpdatedTime` datetime DEFAULT NULL,
  `rolmDeletedUserId` varchar(25) DEFAULT NULL,
  `rolmDeletedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`rolmId`)
) ENGINE=InnoDB AUTO_INCREMENT=8049 DEFAULT CHARSET=utf8 COMMENT='Daftar Role Menu';

/*Data for the table `role_menu` */

insert  into `role_menu`(`rolmId`,`rolmRoleId`,`rolmMenuId`,`rolmView`,`rolmNew`,`rolmEdit`,`rolmDelete`,`rolmConfirm`,`rolmApprove`,`rolmVoid`,`rolmCreatedUserId`,`rolmCreatedTime`,`rolmUpdatedUserId`,`rolmUpdatedTime`,`rolmDeletedUserId`,`rolmDeletedTime`) values 
(6999,5,169,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7000,5,170,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7001,5,171,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7002,5,183,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7003,5,190,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7004,5,191,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7005,5,198,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7006,5,200,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7007,5,209,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7008,5,210,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7029,8,179,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7030,8,180,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7031,8,181,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7032,8,186,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7033,8,196,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7034,8,197,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7035,8,205,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7036,8,206,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7037,8,215,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7038,8,216,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7704,4,2,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7705,4,135,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7706,4,126,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7707,4,132,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7708,4,152,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7709,4,153,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7710,4,154,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7711,4,157,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7723,3,2,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7724,3,135,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7725,3,138,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7726,3,147,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7727,3,155,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7728,3,156,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7782,10,2,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7783,10,135,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7784,10,141,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7785,10,138,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7786,10,139,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7787,10,155,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7788,10,146,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7940,7,135,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7941,7,157,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7942,6,135,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7943,6,156,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7944,2,2,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7945,2,135,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7946,2,138,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7947,2,147,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7948,2,155,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(7949,2,160,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8036,1,2,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8037,1,7,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8038,1,164,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8039,1,165,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8040,1,167,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8041,1,154,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8042,1,162,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8043,1,3,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8044,1,8,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8045,1,14,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8046,1,15,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8047,1,166,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL),
(8048,1,168,0,0,0,0,0,0,0,NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL);

/*Table structure for table `trconfnomor` */

DROP TABLE IF EXISTS `trconfnomor`;

CREATE TABLE `trconfnomor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noKKP` int(11) NOT NULL,
  `noDraft` int(11) NOT NULL,
  `noLHP` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `trconfnomor` */

insert  into `trconfnomor`(`id`,`noKKP`,`noDraft`,`noLHP`) values 
(1,2,0,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
