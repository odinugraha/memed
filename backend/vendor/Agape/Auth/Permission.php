<?php
/**
 * Permission per module authentication
 */
namespace Agape\Auth;

use DB;
use Session;

class Permission {
    /**
    * Get permission of this module
    * 
    * @return Array
    */
    public static function all($menuLink)
    {
        if($menuLink == null){
            return array(
                "view"      => 1,
                "new"       => 1,
                "edit"      => 1,
                "delete"    => 1,
                "confirm"   => 1,
                "void"      => 1,
                "approve"   => 1
            );
        }
        
        $data = (array) DB::table('role_menu')
                ->join('menu', 'menuId','=','rolmMenuId')
                ->where('rolmRoleId', '=', Session::get('userRolhId'))
                ->where('menuLink', '=', $menuLink)
                ->where('menuNonActive', '=', 0)
                ->first();
        
        if(empty($data)){
            return array(
                "view"      => 0,
                "new"       => 0,
                "edit"      => 0,
                "delete"    => 0,
                "confirm"   => 0,
                "approve"   => 0,
                "void"      => 0
            );
        }
        
        return array(
            "view"      => $data['rolmView'],
            "new"       => $data['rolmNew'],
            "edit"      => $data['rolmEdit'],
            "delete"    => $data['rolmDelete'],
            "confirm"   => $data['rolmConfirm'],
            "approve"   => $data['rolmApprove'],
            "void"      => $data['rolmVoid']
        );
    }
    
    /**
    * Get View permission of this module
    * 
    * @param string $menuLink menu link 
    * @return boolean
    */
    public static function view($menuLink)
    {
        if($menuLink == null){
            return 1;
        }
        
        $data = (array) DB::table('role_menu')
                ->select('rolmView')
                ->join('menu', 'menuId','=','rolmMenuId')
                ->where('rolmRoleId', '=', Session::get('userRolhId'))
                ->where('menuLink', '=', $menuLink)
                ->where('menuNonActive', '=', 0)
                ->first();
        
        if(empty($data)){
            return 0;
        }
        
        return $data['rolmView'];
    }
    
    /**
    * Get New permission of this module
    * 
    * @param string $menuLink menu link 
    * @return boolean
    */
    public static function store($menuLink)
    {
        if($menuLink == null){
            return 1;
        }
        
        $data = (array) DB::table('role_menu')
                ->select('rolmNew')
                ->join('menu', 'menuId','=','rolmMenuId')
                ->where('rolmRoleId', '=', Session::get('userRolhId'))
                ->where('menuLink', '=', $menuLink)
                ->where('menuNonActive', '=', 0)
                ->first();
        
        if(empty($data)){
            return 0;
        }
        
        return $data['rolmNew'];
    }
    
    /**
    * Get Edit permission of this module
    * 
    * @param string $menuLink menu link 
    * @return boolean
    */
    public static function update($menuLink)
    {
        if($menuLink == null){
            return 1;
        }
        
        $data = (array) DB::table('role_menu')
                ->select('rolmEdit')
                ->join('menu', 'menuId','=','rolmMenuId')
                ->where('rolmRoleId', '=', Session::get('userRolhId'))
                ->where('menuLink', '=', $menuLink)
                ->where('menuNonActive', '=', 0)
                ->first();
        
        if(empty($data)){
            return 0;
        }
        
        return $data['rolmEdit'];
    }
    
    /**
    * Get Delete permission of this module
    * 
    * @param string $menuLink menu link 
    * @return boolean
    */
    public static function delete($menuLink)
    {
        if($menuLink == null){
            return 1;
        }
        
        $data = (array) DB::table('role_menu')
                ->select('rolmDelete')
                ->join('menu', 'menuId','=','rolmMenuId')
                ->where('rolmRoleId', '=', Session::get('userRolhId'))
                ->where('menuLink', '=', $menuLink)
                ->where('menuNonActive', '=', 0)
                ->first();
        
        if(empty($data)){
            return 0;
        }
        
        return $data['rolmDelete'];
    }
    
    /**
    * Get Confirm permission of this module
    * 
    * @param string $menuLink menu link 
    * @return boolean
    */
    public static function confirm($menuLink)
    {
        if($menuLink == null){
            return 1;
        }
        
        $data = (array) DB::table('role_menu')
                ->select('rolmConfirm')
                ->join('menu', 'menuId','=','rolmMenuId')
                ->where('rolmRoleId', '=', Session::get('userRolhId'))
                ->where('menuLink', '=', $menuLink)
                ->where('menuNonActive', '=', 0)
                ->where('menuNonActive', '=', 0)
                ->first();
        
        if(empty($data)){
            return 0;
        }
        
        return $data['rolmConfirm'];
    }
    
    /**
    * Get Approve permission of this module
    * 
    * @param string $menuLink menu link 
    * @return boolean
    */
    public static function approve($menuLink)
    {
        if($menuLink == null){
            return 1;
        }
        
        $data = (array) DB::table('role_menu')
                ->select('rolmApprove')
                ->join('menu', 'menuId','=','rolmMenuId')
                ->where('rolmRoleId', '=', Session::get('userRolhId'))
                ->where('menuLink', '=', $menuLink)
                ->where('menuNonActive', '=', 0)
                ->where('menuNonActive', '=', 0)
                ->first();
        
        if(empty($data)){
            return 0;
        }
        
        return $data['rolmApprove'];
    }
    
    /**
    * Get Void permission of this module
    * 
    * @param string $menuLink menu link 
    * @return boolean
    */
    public static function void($menuLink)
    {
        if($menuLink == null){
            return 1;
        }
        
        $data = (array) DB::table('role_menu')
                ->select('rolmVoid')
                ->join('menu', 'menuId','=','rolmMenuId')
                ->where('rolmRoleId', '=', Session::get('userRolhId'))
                ->where('menuLink', '=', $menuLink)
                ->where('menuNonActive', '=', 0)
                ->first();
        
        if(empty($data)){
            return 0;
        }
        
        return $data['rolmVoid'];
    }
}