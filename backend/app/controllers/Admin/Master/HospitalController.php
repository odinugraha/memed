<?php
namespace Admin\Master;

use BasicController;
use DB;
use Lang;
use Input;

class HospitalController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new Hospital();
     }
     public function index()
     {
      $param=Input::all();        
       $search=$param['search']['value'];
       try {
            $query = DB::table($this->model->getTable())
                    ->select('*')
                    ->where('mshospitalName','like','%'.$search.'%')
                    ;            
           return $this->getDataGrid($query);                
          }catch(Exception $e){
           return Response::exception($e);
       }    
     }
}