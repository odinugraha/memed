<?php
/*
 * Langguage mapping for Role
 */

return array( 
       "form"=>array(
                    array(
                            'id'    => 'mshospitalId',
                            'name'  => 'KODE',
                            'type'  => 'hidden',
                            'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'mshospitalName',
                            'name'  => 'Nama RS',
                            'type'  => 'text',
                            'readonly'  => '0',
                            ),
                    array(
                            'id'    => 'mshospitalAddress',
                            'name'  => 'Alamat',
                            'type'  => 'text',
                            'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'mshospitalLong',
                            'name'  => 'Longitude',
                            'type'  => 'text',
                            'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'mshospitalLat',
                            'name'  => 'Latitude',
                            'type'  => 'text',
                            'readonly'  => '0',
                        ),
                        
                )
);
