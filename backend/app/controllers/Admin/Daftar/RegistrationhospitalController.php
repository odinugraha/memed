<?php
namespace Admin\Daftar;

use BasicController;
use DB;
use Lang;
use Input;
use Date;

class RegistrationhospitalController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
        $param=Input::all();
        $this->model = new \Admin\Master\Registration();
     }

     public function index(){
           $param=Input::all();

           try {
                $query = DB::table($this->model->getTable())
                        ->select('*',
                        DB::raw('case when regStat=1 then concat("<label class=\"badge badge-inverse-info\">Proses</label>") 
                                      when regStat=2 then concat("<label class=\"badge badge-inverse-danger\">Dibatalkan</label>")
                                      when regStat=3 then concat("<label class=\"badge badge-inverse-success\">Selesai</label>") end as status'))
                        ->join('mspoly','mspolyId','=','regpolyId')
                        ->join('mshospital','mshospitalId','=','reghospitalId')
                        ->join('doc_sch','doc_schId','=','regDoctor')
                        ->orderby('regId','desc');

            $res=$this->getDataGrid($query);   
            return $res;                
                 
           }catch(Exception $e){
               return Response::exception($e);
           }

     }


     public function beforeStore(){
        $param=Input::all();
        $id=Session::get('userIdAdmin');
        $idTrans=$id.'-'.$this->getNoTrans($param['hospital']).'-'.date('Y');

        Input::merge(array( 
                            'regCode'=>$idTrans,
                            'regUserId'=>$id,
                            'regDoctor'=>$param['doctor'],
                            'regPolyId'=>$param['poly'],
                            'regHospitalId'=>$param['hospital'],
                            'regStat'=>$param['stat'],
                          ));                    

     }
     public function getNoTrans($gudang){
            $query = DB::table('trconfnomor')
                    ->select('noRegis')
                    ->limit(1)
                    ->get();            
            
                $nomor=$query[0]->noRegis;
                $nomor ++;
                $q2=DB::table('trconfnomor')
                    ->update(array('noRegis' => $nomor));        
            
            return $this->tambahNol($nomor);
     }

     public function tambahNol($nomor){
        $res=$nomor;
        if(strlen($nomor)==1){
            $res='00000'.$nomor;
        }elseif(strlen($nomor)==2){
            $res='0000'.$nomor;
        }elseif(strlen($nomor)==3){
            $res='000'.$nomor;
        }elseif(strlen($nomor)==4){
            $res='00'.$nomor;
        }elseif(strlen($nomor)==5){
            $res='0'.$nomor;
        }elseif(strlen($nomor)==6){
            $res=$nomor;
        }
        return $res;
     }


     public function reversdate($tanggal){
        if(substr($tanggal, 2,1)=="/" or substr($tanggal, 1,1)=="/"){
            $a=explode("/",$tanggal);
            $result=$a[2].'-'.$a[1].'-'.$a[0];
        }elseif(substr($tanggal, 2,1)=="-" or substr($tanggal, 1,1)=="-"){
            $a=explode("-",$tanggal);
            $result=$a[2].'-'.$a[1].'-'.$a[0];
        }else{
            $result=$tanggal; 
        }
        return $result;
     }


}