<?php
/*
 * Langguage mapping for Role
 */

return array( 
       "form"=>array(
                    array(
                            'id'    => 'mspolyId',
                            'name'  => 'KODE',
                            'type'  => 'hidden',
                            'readonly'  => '0',
                        ),
                    array(
                        'id'    => 'mspolyHospital',
                        'name'  => 'Nama RS',
                        'type'  => 'autocomplete',
                        'comboapi'  => 'backend/public/api/admin/master/Hospitalcombo',
                        'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'mspolyName',
                            'name'  => 'Nama Poli',
                            'type'  => 'text',
                            'readonly'  => '0',
                        ),
                        
                )
);
