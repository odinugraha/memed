<?php
namespace Admin\Master;
use BasicController;
use DB;
use Lang;
use Input;

class PolyhostcomboController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new Poly();
     }
     public function index(){
          $param=Input::all();
          $param['term']=!empty($param['term'])? $param['term'] :'';
          $param['kode']=!empty($param['kode'])? $param['kode'] :'';

           try {
                $query = DB::table($this->model->getTable())
                        ->select('mspolyId as id','mspolyId as kode',
                            DB::raw('concat(mshospitalName," - ",mspolyName) as nama'),
                            DB::raw('concat(mshospitalName," - ",mspolyName) as text'))
                        ->join('mshospital','mshospitalId','=','mspolyHospital')
                        ->where('mspolyName','like','%'.$param['term'].'%')
                        ->where('mshospitalName','like','%'.$param['kode'].'%')
                        ->limit(100)
                        ->get();
                
               return $query;                
           }catch(Exception $e){
               return Response::exception($e);
           }

     }
}