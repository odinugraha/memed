<?php
/*
 * Langguage mapping for Role
 */

return array( 
       "form"=>array(
                    array(
                            'id'    => 'regId',
                            'name'  => 'KODE',
                            'type'  => 'hidden',
                            'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'regStat',
                            'name'  => 'KODE',
                            'type'  => 'hidden',
                            'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'regHospitalId',
                            'name'  => 'Rumah Sakit',
                            'type'  => 'autocomplete',
                            'comboapi'  => 'backend/public/api/admin/master/Hospitalcombo',
                            'readonly'  => '1',
                        ),
                    array(
                            'id'    => 'regPolyId',
                            'name'  => 'Poli',
                            'type'  => 'autocomplete',
                            'comboapi'  => 'backend/public/api/admin/master/Polycombo',
                            'readonly'  => '1',
                        ),
                    array(
                            'id'    => 'regDoctor',
                            'name'  => 'Doctor',
                            'type'  => 'autocomplete',
                            'comboapi'  => 'backend/public/api/admin/daftar/Doctorlistcombo',
                            'readonly'  => '1',
                    ),
                        
                )
);
