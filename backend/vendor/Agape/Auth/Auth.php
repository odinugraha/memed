<?php
/**
 * Auth Login
 */
namespace Agape\Auth;

use DB;
use Session;
use Hash;
use Illuminate\Support\Facades\Auth as iAuth;

class Auth extends iAuth{
    
    public function attemp($user) 
    {
        $data = (array) DB::table('user')
                ->where('userName', '=', $user['username'])
                ->first();
        
        if(empty($data)){
            return false;
        }
        
        if (!Hash::check($user['password'], $data['userPassword'])) 
        {
            return false;
        }
        
        return $data;
    }


    public function attempAdmin($user) 
    {
        $data = (array) DB::table('admin_users')
                ->where('ausrUsername', '=', $user['username'])
                ->first();
        
        if(empty($data)){
            return false;
        }
        
        if (!Hash::check($user['password'], $data['ausrPassword'])) 
        {
            return false;
        }
        
        return $data;
    }

    
    /**
    * Determine if the current user is authenticated.
    *
    * @return bool
    */
    public function check()
    {
            return ! is_null($this->user());
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
            return ! $this->check();
    }
    
    public function logout() 
    {
        //Session::flush();
    }
}
