<?php
/*
 * Langguage mapping for Role
 */

return array( 
       "form"=>array(
                    array(
                            'id'    => 'ausrId',
                            'name'  => 'Id',
                            'type'  => 'hidden',
                            'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'ausrUsername',
                            'name'  => 'Username',
                            'type'  => 'text',
                            'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'ausrName',
                            'name'  => 'Nama',
                            'type'  => 'text',
                            'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'ausrPassword',
                            'name'  => 'Password',
                            'type'  => 'password',
                            'readonly'  => '0',
                        ),
                    array(
                            'id'    => 'ausrRolhId',
                            'name'  => 'Role',
                            'type'  => 'autocomplete',
                            'comboapi'  => 'backend/public/api/admin/config/roleforcombo',
                            'readonly'  => '0',
                    ),
                    array(
                        'id'    => 'ausrNik',
                        'name'  => 'NIK',
                        'type'  => 'text',
                        'readonly'  => '0',
                    ),
                    array(
                        'id'    => 'ausrBirthdate',
                        'name'  => 'Tanggal Lahir',
                        'type'  => 'text',
                        'readonly'  => '0',
                    ),
                    array(
                        'id'    => 'ausrAddress',
                        'name'  => 'Alamat',
                        'type'  => 'text',
                        'readonly'  => '0',
                    ),
                    array(
                        'id'    => 'ausrPhone',
                        'name'  => 'No Telp',
                        'type'  => 'text',
                        'readonly'  => '0',
                    ),
                    array(
                        'id'    => 'ausrProvince',
                        'name'  => 'Provinsi',
                        'type'  => 'autocomplete',
                        'comboapi'  => 'backend/public/api/admin/master/Provcombo',
                        'readonly'  => '0',
                    ),
                    array(
                        'id'    => 'ausrKab',
                        'name'  => 'Kabupaten',
                        'type'  => 'autocomplete',
                        'comboapi'  => 'backend/public/api/admin/master/Kabcombo',
                        'readonly'  => '0',
                    ),  
                    array(
                        'id'    => 'ausrKec',
                        'name'  => 'Kecamatan',
                        'type'  => 'autocomplete',
                        'comboapi'  => 'backend/public/api/admin/master/Keccombo',
                        'readonly'  => '0',
                    ), 
                    array(
                        'id'    => 'ausrKel',
                        'name'  => 'Kelurahan',
                        'type'  => 'autocomplete',
                        'comboapi'  => 'backend/public/api/admin/master/Kelcombo',
                        'readonly'  => '0',
                    ),  
                )
);
