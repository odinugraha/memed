<?php
namespace Admin\Master;

use BasicModels;

class Schedule extends BasicModels {

  protected $table='doc_sch';

        /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
    protected $fillable=array('doc_schName','doc_schNumb','doc_schDay','doc_schPeriodStart','doc_schPeriodStop','doc_schPoly');
        
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
    protected $primaryKey = 'doc_schId';

    public $timestamps = true;
   public $companystamps = false;
   
   /**
    * The name of the "created at" column.
    *
    * @var string
    */
   const CREATED_AT = 'doc_schCreateTime';
   
   /**
    * The name of the "created by" column.
    *
    * @var string
    */
   const CREATED_BY = 'doc_schCreateUser';

   /**
    * The name of the "updated at" column.
    *
    * @var string
    */
   const UPDATED_AT = 'doc_schUpdateTime';
   
   /**
    * The name of the "updated by" column.
    *
    * @var string
    */
   const UPDATED_BY = 'doc_schUpdateUser';
   
   /**
    * The name of the "deleted at" column.
    *
    * @var string
    */
   const DELETED_AT = 'doc_schDeleteTime';
   
   /**
    * The name of the "deleted by" column.
    *
    * @var string
    */
   const DELETED_BY = 'doc_schDeleteUser';

}