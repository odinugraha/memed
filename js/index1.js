$(document).ready(function(){

    cekSession();    

	var host = window.location.host;
    $.ajax({
        type: "GET",
        url: "backend/public/getSession",
        dataType:"json",
        async: false,        
        success:function(data){
            //console.log(data);
            if(data.userNameAdmin==''){
                window.location.href = 'home.html';
            }
            $('#longname').html(data.userNameAdminLong);
            $('#shortname').html(data.userNameAdmin);
            document.getElementById('namapendek').value=data.userNameAdmin;
            document.getElementById('idnya').value=data.unit;
        }
    });      

    $.ajax({
        type: "GET",
        url: 'backend/public/api/admin/config/menuadmin',
        dataType:"json",
        async: false,        
        success:function(data){
            //console.log(data);
            var level1=data.data;
            var result='<ul class="nav main-menu">';
            for(i=0;i<=data.data.length-1;i++){
                
                level2=level1[i].data;
                result +='<li class="dropdown">';
                result +='    <a href="'+level1[i].htmlLink+'" class="dropdown-toggle">';
                result +='        <i class="fa '+level1[i].menuIcon+'"></i>';
                result +='        <span class="hidden-xs">'+level1[i].menuName+'</span>';
                result +='    </a>';
                
                if(level2.length>0){
                    result +='<ul class="dropdown-menu">';
                    for(j=0;j<=level2.length-1;j++){
                        
                        var level3=level2[j].data;
                        if(level3.length>0){
                            result +='<li class="dropdown">';
                            result +='    <a href="#" class="dropdown-toggle">';
                            result +='        <i class="fa fa-plus-square"></i>';
                            result +='        <span class="hidden-xs">'+level2[j].menuName+'</span>';
                            result +='    </a>';
                            result +='    <ul class="dropdown-menu">';
                            for(h=0;h<=level3.length-1;h++){
                                var level4=level3[h].data;
                                if(level4.length>0){

                                    result +='<li class="dropdown">';
                                    result +='    <a href="#" class="dropdown-toggle">';
                                    result +='        &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-plus-square"></i>';
                                    result +='        <span class="hidden-xs">'+level3[h].menuName+'</span>';
                                    result +='    </a>';
                                    result +='    <ul class="dropdown-menu">';
                                    
                                    
                                    for(k=0;k<=level4.length-1;k++){
                                        //console.log(level4);
                                        result +='<li><a class="ajax-link" href="'+level4[k].htmlLink+'#'+level4[k].menuLink+'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+level4[k].menuName+'</a></li>';
                                    }
                                    result +='    </ul>';
                                    result +='</li>';

                                }else{
                                    result +='<li><a class="ajax-link" href="'+level3[h].htmlLink+'#'+level3[h].menuLink+'">&nbsp;&nbsp;&nbsp;&nbsp;'+level3[h].menuName+'</a></li>';
                                }

                            }
                            result +='    </ul>';
                            result +='</li>';

                        }else{
                            result +='<li><a class="ajax-link" href="'+level2[j].htmlLink+'#'+level2[j].menuLink+'">'+level2[j].menuName+'</a></li>';

                        }
                    }
                    result +='</ul>';
                }

                result +='</li>';    
            }            
            result +='</ul>';
            $('#lmenu').html(result);
        }
    });    


});	

function cekSession(){

    setInterval(function(){ 

        $.ajax({
            type: "GET",
            url: "backend/public/getSession",
            dataType:"json",
            async: false,        
            success:function(data){
                if(data.userNameAdmin==''){
                    logout();
                }
            }
        });      

    }, 5000);

}

// var x = 1;
// var lastResponse = '';
// function cek(){ 

//     var unit='';
//     $.ajax({
//         type: "GET",
//         url: "backend/public/getSession",
//         dataType:"json",
//         async:false,
//         success:function(data){            
//             unit=data.unit;
//         }
//     });          

//     if(unit==2){
//        var link="backend/public/api/admin/gudang/notifko";
//     }else if(unit==6){
//         var link="backend/public/api/admin/gudang/notiffs";
//     }
//     else if(unit==4){
//         var link="backend/public/api/admin/gudang/notifranap";
//     }
//     else if(unit==5){
//         var link="backend/public/api/admin/gudang/notifrajal";
//     }
//     else{
//         var link="backend/public/api/admin/gudang/notifkosong";        
//     }

//     $.ajax({
//         url: link, 
//         cache: false,
//         dataType:'json',
//         success: function(data){
//           $('#jmlh').html(data[0].notif);
//           if (lastResponse && data[0].notif !== lastResponse) {
//             var audio = new Audio('sound/arpeggio.ogg')
//             audio.play()
//           }
//           lastResponse = data[0].notif

//         }
//     })
//     var waktu = setTimeout("cek()",20000);
//     }

// $(document).ready(function(){
//     cek();
//     var unit='';
//     $.ajax({
//         type: "GET",
//         url: "backend/public/getSession",
//         dataType:"json",
//         async:false,
//         success:function(data){            
//             unit=data.unit;
//         }
//     }); 
//     if(unit==2){
//        var link2="backend/public/api/admin/gudang/notifmessageko";
//     }else if(unit==6){
//         var link2="backend/public/api/admin/gudang/notifmessagefs";
//     }
//     else if(unit==4){
//         var link2="backend/public/api/admin/gudang/notifmessageranap";
//     }
//     else if(unit==5){
//         var link2="backend/public/api/admin/gudang/notifmessagerajal";
//     }else{
//         var link2="backend/public/api/admin/gudang/notifmessagekosong";        
//     }
//     $("#notif").click(function(){
//         if(x==1){
//             $("#notif").css("background-color","#efefef");
//             x = 0;
//         }else{
//             $("#notif").css("background-color","#e0e0e0");
//             x = 1;
//         }
//         $("#info").toggle();
//         $.ajax({
//             url: link2,
//             cache: false,
//             dataType : 'json',
//             success: function(data){
//                 var res='';
//                 for(i=0;i<=data.length-1;i++){     
//                         res +='    <i style="font-size: 15px" class="fa fa-bell"></i>';
//                         res +='    <span style="font-size: 13px">'+data[i].notif+'</span></br>';
//                         res +='    <i style="font-size: 10px" class="fa fa-clock-o"></i>';
//                         res +='    <span style="font-size: 10px; ">'+data[i].waktu+'</span></br>';                                           
                        

//                 }
//                 $("#konten-info").html(res);
//             }
//         });

//     });
//     $("#content").click(function(){
//         $("#info").hide();
//         $("#notif").css("background-color","#f7f7f7");
//         x = 1;
//     });
// }); 


function logout(){
		var host = window.location.host;

        $.ajax({
            type: "GET",
            url: 'backend/public/api/admin/logout',
            dataType:"json",
            success:function(data){
                //console.log(data);
                window.location.href = 'home.html';
            }
        });
}

