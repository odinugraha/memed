<?php
namespace Admin\Master;

use BasicModels;

class Callregis extends BasicModels {

  protected $table='callregis';

        /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
    protected $fillable=array('callregisCode','callregisUserId','callregisDoctor','callregisPolyId','callregisHospitalId','callregisStat');
        
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
    protected $primaryKey = 'callregisId';

    public $timestamps = true;
   public $companystamps = false;
   
   /**
    * The name of the "created at" column.
    *
    * @var string
    */
   const CREATED_AT = 'callregisCreateTime';
   
   /**
    * The name of the "created by" column.
    *
    * @var string
    */
   const CREATED_BY = 'callregisCreateUser';

   /**
    * The name of the "updated at" column.
    *
    * @var string
    */
   const UPDATED_AT = 'callregisUpdateTime';
   
   /**
    * The name of the "updated by" column.
    *
    * @var string
    */
   const UPDATED_BY = 'callregisUpdateUser';
   
   /**
    * The name of the "deleted at" column.
    *
    * @var string
    */
   const DELETED_AT = 'callregisDeleteTime';
   
   /**
    * The name of the "deleted by" column.
    *
    * @var string
    */
   const DELETED_BY = 'callregisDeleteUser';

}