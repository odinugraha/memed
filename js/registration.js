function setFieldType(){
    var field=[
                {
                    nama:'idcard',
                    readonly:false,
                    type:'angka',
                },
                {
                    nama:'fullname',
                    readonly:false,
                    type:'text',
                },
                {
                    nama:'birthdate',
                    readonly:false,
                    type:'text',
                },
                {
                    nama:'address',
                    readonly:false,
                    type:'text',
                },
                {
                    nama:'province',
                    readonly:false,
                    type:'text',
                },
                {
                    nama:'district',
                    readonly:false,
                    type:'text',
                },
                {
                    nama:'subdistrict',
                    readonly:false,
                    type:'text',
                },
                {
                    nama:'village',
                    readonly:false,
                    type:'text',
                },
                {
                    nama:'hospital',
                    readonly:false,
                    apidata:'backend/public/api/admin/master/hospitalcombo',
                    type:'autocomplete',
                },
                {
                    nama:'poly',
                    readonly:false,
                    apidata:'backend/public/api/admin/master/polycombo',
                    type:'autocomplete2',
                },



              ];

    for (i = 0; i <= field.length-1; i++) {
       
        if(field[i].type=='text'){            
            if(field[i].readonly){
                $('#'+field[i].nama).attr("readonly","readonly");
            }
        }

        if(field[i].type=='date'){
            
            var tg=new Date();
            var day=tg.getDate();
            var mon=tg.getMonth();
            var yee=tg.getFullYear();
            mon=mon+1;

            var tanggal=mon+'/'+day+'/'+yee;    
            $('#'+field[i].nama).attr('value',tanggal);


            $('#'+field[i].nama).datepicker({
                format: 'dd/mm/yyyy',
            });    
            if(field[i].readonly){
                $('#'+field[i].nama).attr("readonly","readonly");
            }
        }
    
        if(field[i].type=='angka'){
            $('#'+field[i].nama).keypress(function(){
              return(currency(event));
            });
            if(field[i].readonly){
                $('#'+field[i].nama).attr("readonly","readonly");
            }
        }    

        if(field[i].type=='autocomplete'){
            var comboapi=field[i].apidata;

            $('#'+field[i].nama)
                .on("change", function(e) {
                      var id=e.added.id;
                      var txt=e.added.nama;  
                      var kode=e.added.kode;  
                      var satuan=e.added.satuan;  
                      var nmhidden=this.id+'_hidden';
                      $('#'+nmhidden).attr('value',txt);
                })      
                .select2({
                placeholder: "Pilih Data", 
                ajax: {
                        url: comboapi,
                        dataType: 'json',
                        quietMillis: 100,
                        data: function (term, page) {
                            return {
                                term: term, //search term
                                page_limit: 10 // page size
                            };
                        },
                        results: function (data, page) {
                            return { results: data };
                        },
                    }
            });
        }
        if(field[i].type=='autocomplete2'){
            var comboapi=field[i].apidata;

            $('#'+field[i].nama)
                .on("change", function(e) {
                      var id=e.added.id;
                      var txt=e.added.nama;  
                      var kode=e.added.kode;  
                      var satuan=e.added.satuan;  
                      var nmhidden=this.id+'_hidden';
                      $('#'+nmhidden).attr('value',txt);
                })      
                .select2({
                placeholder: "Pilih Data", 
                ajax: {
                        url: comboapi,
                        dataType: 'json',
                        quietMillis: 100,
                        data: function (term, page) {
                            return {
                                term: term, //search term
                                page_limit: 10 // page size
                            };
                        },
                        results: function (data, page) {
                            return { results: data };
                        },
                    }
            });
        }  
    }    
} 