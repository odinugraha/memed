function setFieldType(){
    var field=[
    			{
                    nama:'userid',
                    readonly:false,
                    type:'angka',
                },
                {
                    nama:'nik',
                    readonly:false,
                    type:'angka',
                },
                {
                    nama:'name',
                    readonly:false,
                    type:'text',
                },
                {
                    nama:'birthdate',
                    readonly:true,
                    type:'text',
                },
                {
                    nama:'address',
                    readonly:true,
                    type:'text',
                },
                {
                    nama:'phonenumb',
                    readonly:true,
                    type:'angka',
                },
                {
                    nama:'province',
                    readonly:true,
                    type:'text',
                },
                {
                    nama:'district',
                    readonly:true,
                    type:'text',
                },
                {
                    nama:'sub-district',
                    readonly:true,
                    type:'text',
                },
                {
                    nama:'village',
                    readonly:true,
                    type:'date',
                },

                {
                    nama:'hospital',
                    readonly:false,
                    apidata:'backend/public/api/admin/master/Hospitalcombo',
                    type:'autocomplete',
                },
                {
                    nama:'poly',
                    readonly:false,
                    apidata:'backend/public/api/admin/master/Polycombo',
                    type:'autocomplete',
                },
                {
                    nama:'docsch',
                    readonly:false,
                    apidata:'backend/public/api/admin/master/Doctorcliniccombo',
                    type:'autocomplete',
                },
                // {
                //     nama:'doctor',
                //     readonly:false,
                //     apidata:'',
                //     type:'autocomplete2',
                // },
              ];

    for (i = 0; i <= field.length-1; i++) {
       
        if(field[i].type=='text'){            
            if(field[i].readonly){
                $('#'+field[i].nama).attr("readonly","readonly");
            }
        }

        if(field[i].type=='date'){
            
            var tg=new Date();
            var day=tg.getDate();
            var mon=tg.getMonth();
            var yee=tg.getFullYear();
            mon=mon+1;

            var tanggal=mon+'/'+day+'/'+yee;    
            $('#'+field[i].nama).attr('value',tanggal);


            // $('#'+field[i].nama).datepicker({
            //     format: 'dd/mm/yyyy',
            // });    
            if(field[i].readonly){
                $('#'+field[i].nama).attr("readonly","readonly");
            }
        }
    
        if(field[i].type=='angka'){
            $('#'+field[i].nama).keypress(function(){
              return(currency(event));
            });
            if(field[i].readonly){
                $('#'+field[i].nama).attr("readonly","readonly");
            }
        }    

        if(field[i].type=='autocomplete'){
            var comboapi=field[i].apidata;

            $('#'+field[i].nama)
                .on("change", function(e) {
                      var id=e.added.id;
                      var txt=e.added.nama;  
                      var kode=e.added.kode;  
                      var satuan=e.added.satuan;  
                      var nmhidden=this.id+'_hidden';
                      $('#'+nmhidden).attr('value',txt);
                      if(this.id=='hospital'){
                        // document.getElementById('addidfarmasi').value=e.added.barangId;
                        // document.getElementById('addkodefarmasi').value=kode;
                        // document.getElementById('addsatuanfarmasi').value=satuan;
                      }
                      if(this.id=='poli'){
                        // document.getElementById('addidproduksi').value=e.added.barangId;
                        // document.getElementById('addkodeproduksi').value=kode;
                        // document.getElementById('addsatuanproduksi').value=satuan;
                      }

                })      
                .select2({
                placeholder: "Pilih Data", 
                ajax: {
                        url: comboapi,
                        dataType: 'json',
                        quietMillis: 100,
                        data: function (term, page) {
                            return {
                                term: term, //search term
                                page_limit: 10 // page size
                            };
                        },
                        results: function (data, page) {
                            return { results: data };
                        },
                    }
            });
        } 
    }      



}

function getLoc(features,arr){
    $.ajax({
        type: "GET",
        url : "backend/public/api/admin/master/hospitalloc",
        dataType:"json",
        success:function(features){
            // console.log(features)
            // var no = data.data.length-1;
            // var coordinates='';
            // for(i=0;i<=no;i++){
            //     lat=data.data[i].mshospitalLat;
            //     long=data.data[i].mshospitalLong;
            //     coordinates=[long,lat];
            // }
            // // var geojson={}
            // var lngdest='';
            // var latdest='';
            mapInit(features,arr);
            
        }    
    });          
}

function detailreg(){
    var status=document.getElementById("regis_status").value;
    // if(status==""){}
    $.ajax({
        type: "GET",
        url : "backend/public/api/admin/daftar/registration",
        dataType:"json",
        success:function(data){
            var no = data.data.length-1;
            console.log(no);
            console.log(data);
            if(no<0){
                setFieldType();
                document.getElementById("service").style.display = "block";
                document.getElementById("regis_status").value='0';
                document.getElementById('btn').innerHTML ='DAFTAR';
                document.getElementById('btn').style='background-color:#2ABA66; color:#fffaf9';
            }
            else{
                document.getElementById("service").style.display = "none";
                document.getElementById("regis_status").value='1';
                document.getElementById('btn').innerHTML ='CANCEL';
                document.getElementById('btn').style='background-color:#FF0017; color:#fffaf9';
                document.getElementById('regid').value=data.data[0].regId;
            }
            ;
        }    
    }); 
     
}



function geoMap(features,arr){
        var geojson=features;

        // $.ajax({
        // type: "GET",
        // url : "backend/public/api/admin/master/location",
        // dataType:"json",
        // success:function(features){
        //     // console.log(features);
        //     //     // geoMap(features);
        //     geojson=features;
        // var lng_dest=lng_dest;
        // var lat_dest=lat_dest;
        console.log(geojson);
        // console.log(lng_dest);
        // console.log(lat_dest);
        // var lati=document.getElementById('lat_origin').value;
        // var longi=document.getElementById('long_origin').value;
        var saved_markers = [113.69020958878696, -8.173937981580835];
        var user_location = [longi, lati];

        mapboxgl.accessToken = 'pk.eyJ1Ijoic3RyYXNiZXJyeSIsImEiOiJjamR2ZmZlNzkydHRxMnhxb25jbjM2Mmh0In0.BLWYirz-UGQ8yu3JDggg6Q';
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: saved_markers,
            zoom: 7
        });

        var geolocate = new mapboxgl.GeolocateControl({
            positionOptions: {
                        enableHighAccuracy: true
            },
                        trackUserLocation: true
            });
        map.addControl(geolocate);

        var geocoder = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            marker: true,
            mapboxgl: mapboxgl
        });

        
        var marker ;
        
        var directions = new MapboxDirections({
          accessToken: mapboxgl.accessToken,
          interactive: false,
          controls: {
            inputs: false,
            instructions: false,
            profileSwitcher: false
          }
        });
        map.addControl(directions);

        var size = 200;
 
        var pulsingDot = {
        width: size,
        height: size,
        data: new Uint8Array(size * size * 4),
         
        onAdd: function() {
        var canvas = document.createElement('canvas');
        canvas.width = this.width;
        canvas.height = this.height;
        this.context = canvas.getContext('2d');
        },
         
        render: function() {
        var duration = 1000;
        var t = (performance.now() % duration) / duration;
         
        var radius = size / 2 * 0.3;
        var outerRadius = size / 2 * 0.7 * t + radius;
        var context = this.context;
         
        // draw outer circle
        context.clearRect(0, 0, this.width, this.height);
        context.beginPath();
        context.arc(this.width / 2, this.height / 2, outerRadius, 0, Math.PI * 2);
        context.fillStyle = 'rgba(255, 200, 200,' + (1 - t) + ')';
        context.fill();
         
        // draw inner circle
        context.beginPath();
        context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);
        context.fillStyle = 'rgba(255, 100, 100, 1)';
        context.strokeStyle = 'white';
        context.lineWidth = 2 + 4 * (1 - t);
        context.fill();
        context.stroke();
         
        // update this image's data with data from the canvas
        this.data = context.getImageData(0, 0, this.width, this.height).data;
         
        // keep the map repainting
        map.triggerRepaint();
         
        // return `true` to let the map know that the image was updated
        return true;
        }
        };

        map.on('load', function() {
            geolocate
            map.repaint = true;
            // addMarker(user_location,'load');
            add_markers(saved_markers);

            directions.setOrigin(user_location);
            // flyToStore(locate);
            
            // document.getElementById('long_origin').value=lng;
            // document.getElementById('lat_origin').value=lat;

            // });
            // directionAssign(lng_dest,lat_dest);
        });
        // function directionAssign(lng_dest,lat_dest){
            // var x=document.getElementById("long_des").value;
            // var y=document.getElementById('lat_origin').value;
            // var origin=x;
            // console.log(y);
        //     console.log([lng_dest,lat_dest])
            
        // }
        var locate1=arr;
        
        console.log(locate1);
        // // [113.668076,-8.184486]
        // if(locate1==[]){
        directions.setDestination(locate1);
        // }
        // else{
        // directions.setDestination(locate1);
        // }
        function addMarker(ltlng,event) {

            if(event === 'click'){
                user_location = ltlng;
            }
            marker = new mapboxgl.Marker({draggable: true,color:"#d02922"})
                .setLngLat(user_location)
                .addTo(map)
                // .on('dragend', onDragEnd);
        }

        function flyToStore(currentFeature) {
            map.flyTo({
              center: currentFeature,
              zoom: 15
            });
          }

        function add_markers(coordinates) {
            
            geojson.features.forEach(function(marker) {

                var el = document.createElement('div');
                    el.className = 'marker';
                    // el.style.backgroundImage = 'url(src/images/locmemed.png/' + marker.properties.iconSize.join('/') + '/)';
                    // el.style.width = marker.properties.iconSize[0] + 'px';
                    // el.style.height = marker.properties.iconSize[1] + 'px';
                 
                    el.addEventListener('click', function() {
                    window.alert(marker.properties.message);
                });
                
                new mapboxgl.Marker(el)
                    .setLngLat(marker.geometry.coordinates)
                    .addTo(map);
                });

        }
    }

    function save(){
    
        var postData=new Object();
        var status=document.getElementById('regis_status').value;
        var id=document.getElementById('regid').value;
        if (status==0) {
            var hospital=document.getElementById('hospital').value;
            var poly=document.getElementById('poly').value;
            var doctor=document.getElementById('docsch').value;

            postData['hospital']=hospital;
            postData['poly']=poly;
            postData['doctor']=doctor;
            postData['stat']=1;

            if(confirm("Anda akan mendaftar ?")){
                $.ajax({
                    type: "POST",
                    url : "backend/public/api/admin/daftar/registration",
                    data : postData, 
                    dataType:"json",
                    success:function(data){
                        detailreg();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert(textStatus+" : " + errorThrown+" -> GAGAL DISIMPAN !!!"); 
                    }
                });  
            }  
        }
        else{
            postData['regStat']=2;
            if(confirm("Anda Akan Simpan Transaksi ?")){
                $.ajax({
                    type: "PUT",
                    url : "backend/public/api/admin/daftar/registration/"+id,
                    data : postData, 
                    dataType:"json",
                    success:function(data){
                        detailreg();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert(textStatus+" : " + errorThrown+" -> GAGAL DISIMPAN !!!"); 
                    }
                });  
            } 
        }
        
                
    
    }