<?php
namespace Admin\Master;

use BasicController;
use DB;
use Lang;
use Input;

class ScheduleController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new Schedule();
     }
     public function index()
     {
      $param=Input::all();        
       $search=$param['search']['value'];
       try {
            $query = DB::table($this->model->getTable())
                    ->select('*')
                    ->join('mspoly','mspolyId','=','doc_schPoly')
                    ->join('mshospital','mshospitalId','=','mspolyHospital')
                    ->where('doc_schName','like','%'.$search.'%')
                    ;            
           return $this->getDataGrid($query);                
          }catch(Exception $e){
           return Response::exception($e);
       }    
     }
}