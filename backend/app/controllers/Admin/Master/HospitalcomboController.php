<?php
namespace Admin\Master;
use BasicController;
use DB;
use Lang;
use Input;

class HospitalcomboController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new Hospital();
     }
     public function index(){
          $param=Input::all();
          $param['term']=!empty($param['term'])? $param['term'] :'';
          $param['kode']=!empty($param['kode'])? $param['kode'] :'';

           try {
                $query = DB::table($this->model->getTable())
                        ->select('mshospitalId as id','mshospitalId as kode','mshospitalName as nama','mshospitalName as text')
                        ->where('mshospitalName','like','%'.$param['term'].'%')
                        ->where('mshospitalId','like','%'.$param['kode'].'%')
                        ->limit(100)
                        ->get();
                
               return $query;                
           }catch(Exception $e){
               return Response::exception($e);
           }

     }
}