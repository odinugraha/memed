<?php
/*
 * Langguage mapping for Role
 */
$data['kolom']= array(
                  array(
                        "title"=>"Kode", 
                        "data"=>"mshospitalId"
                        ),
                  array(
                        "title"=>"Nama Rumah Sakit", 
                        "data"=>"mshospitalName"
                        ),
                  array(
                        "title"=>"Alamat", 
                        "data"=>"mshospitalAddress"
                        ),
                  array(
                        "title"=>"Longitude", 
                        "data"=>"mshospitalLong"
                        ),
                  array(
                        "title"=>"Latitude", 
                        "data"=>"mshospitalLat"
                        ),
                     
                );


return $data;