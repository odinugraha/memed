<?php
/**
 * Auth Login
 */
namespace Agape\Auth;

use DB;
use Session;
use Auth;
use Hash;
use Redirect;
use Lang;
use Config;

class Login {
    
    public function authentication($user)
    {
        $today = new \DateTime(); // Today
        $userDb = $this->attemp($user);
/*
        if(empty($userDb)){
            return 119;
        }
*/
        
        if(empty($userDb)){
            if(Session::has('userWrongInput')){
                if(Session::get('userWrongInput')<=1){
                    $now=Session::get('userWrongInput');
                    $now=$now+1;
                    Session::put('userWrongInput',$now);
                    Session::put('chaptSession',0);
                    return 119; //wrong id or password
                }elseif(Session::get('userWrongInput')==4){
                    DB::update('update user set userBannedTime = "'.date('Y-m-d h:i:s').'" where userName ="'.$user['username'].'" ');
                    Session::put('chaptSession',0);
                    return 9999;
                }

                if(Session::get('userWrongInput')>1){
                    Session::put('chaptSession',1);
                    return 400;
                }
            }else{

                Session::put('userWrongInput',1);
                Session::put('chaptSession',0);
                return 119; //wrong id or password
            }
        }
        
        $id = $userDb['userId'];
        
        if($userDb['userNonActive'] != 0){
            return 118; //inactive user
        }
        
        $start = date_parse($userDb['userValidityStart']);
        $end = date_parse($userDb['userValidityEnd']);
        
        $today = new \DateTime(); // Today
        $dateBegin = new \DateTime($start['year'].'-'.$start['month'].'-'.$start['day']);
        $dateEnd  = new \DateTime($end['year'].'-'.$end['month'].'-'.$end['day']);

        if ($today->getTimestamp() >= $dateBegin->getTimestamp() && 
            $today->getTimestamp() <= $dateEnd->getTimestamp()){
            //
        }else{
           return 121; //login out of allowed time
        }
        
        $companyList = 0;
        
        $comp = $this->getCompanyUser($userDb['userId']);
        if(count($comp) > 1){
            $companyList = 1;
        }
        
        // go to dashboard
        if($companyList == 0 && $userDb['userFirstLogin'] == 0){
            DB::update('update user set userBannedTime = "0000-00-00 00:00:00" where userName ="'.$user['username'].'" ');
            Session::put('userWrongInput',0);
            $this->setSession($userDb, $comp[0]);
            Auth::attempt($user);

            $ceksessionlogin=$this->addUserSession($userDb['userId']);
            if(count($ceksessionlogin)>0){
               return 126; //user already login            
            }

        } else {
            $lang = ($this->lang($userDb['userLanguage']) == '') ? Config::get('app.locale'):$this->lang($userDb['userLanguage']);
            Session::put('tempUserId', $id);
            Session::put('tempPw', $user['password']);
            Session::put('userFirstLogin', $userDb['userFirstLogin']);
            Session::put('companyList', $companyList);
            Session::put('userRolhId', $userDb['userRolhId']);
            Session::put('locale', $lang);
            Config::set( 'app.locale', $lang );
        }
        

        $result = array( 'data' => array(
            'userFirstLogin' => $userDb['userFirstLogin'],
            'companyList' => $companyList
        ));
        
        // update last login date
        $this->updatelastLogin($id);
        return $result;
    }
    
    public function attemp($user) 
    {
        $data = (array) DB::table('user')
                ->where('userName', '=', $user['username'])
                ->first();
        
        if(empty($data)){
            return false;
        }
        
        if (!Hash::check($user['password'], $data['userPassword'])) 
        {
            return false;
        }
        
        return $data;
    }
    
    public function getUser($userId)
    {
        $data = (array) DB::table('user')
                ->where('userId', '=', $userId)
                ->first();
        return $data;
    }
    
    private function getCompanyUser($id)
    {
        $query = (array) DB::table('user_company')
                ->select('compId','compName')
                ->leftJoin('company', 'compId', '=', 'uscoCompId')
                ->where('uscoUserId', $id)
                ->get();
        return $query;
    }
    
    public function getCompany()
    {
        $query = DB::table('company')
                ->select('compId','compName')
                ->where('compId', '=', DB::raw(Session::get('companyId')))
                ->first();
        return $query;
    }
    
    public function setSession($userDb, $comp){
        $lang = ($this->lang($userDb['userLanguage']) == '')?Config::get('app.locale'):$this->lang($userDb['userLanguage']);
        
        Session::put('companyIdUser', $comp->compId);
        Session::put('companyNameUser', $comp->compName);
        Session::put('userIdUser', $userDb['userId']);
        Session::put('userNameUser', $userDb['userName']);
        Session::put('userRolhIdUser', $userDb['userRolhId']);
        Session::put('user', '1');
        Session::put('locale', $lang);
        Config::set( 'app.locale', $lang );   
    }
    
    private function updateLastLogin($userId){
        if($userId > 0){
            $input = array( 'userLastLogin' => date('Y-m-d H:i:s'));
            DB::table('user')
                    ->where('userId', '=', $userId)
                    ->update($input);
        }
    }



    public function addUserSession($userId){

            $log=DB::table('user_sessions')
                    ->where('usesUserId', '=', $userId)
                    ->get();

            if(empty($log)){        
                $data = array('usesUserId' => $userId, 
                        'usesSessionId' => Session::getId(), 
                        'usesComp' => md5($_SERVER['REMOTE_ADDR']), 
                        'usesLastAccess' => date("Y-m-d H:i:s"));
                DB::table('user_sessions')->insert($data);
                return array();
            }else{       
                return $log;                
            }
            
    }

    private function lang($langId) {
        $langName = 'en';
        switch ($langId){
            case "1": $langName = 'en';
                break;
            case "2": $langName = 'id';
                break;
            case "3": $langName = 'ot';
                break;
        }
        
        return $langName;
    }    
}
