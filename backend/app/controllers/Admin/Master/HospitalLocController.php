<?php
namespace Admin\Master;

use BasicController;
use DB;
use Lang;
use Input;

class HospitalLocController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new Hospital();
     }
     public function index()
     {           
      $query = DB::table($this->model->getTable())
              ->select('*')
              ->get();

      // $result['features']=[]$query;
      $i=0;
      foreach($query as $key => $value) {

            $features[] = array(
                    'type' => 'Feature',
                    'geometry' => array('type' => 'Point', 'coordinates' => array($value->mshospitalLong,$value->mshospitalLat)),
                    'properties' => array('phoneFormatted' => '12345','phone'=>'12323','address'=>'12323','city'=>'31232','country'=>'dasd','crossStreet'=>'1323','postalCode'=>'23123','state'=>'dasdsd', 'id' => $value->mshospitalId,'name'=>$value->mshospitalName),
                    );
            $i++;
            };  
      $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

      return  json_encode($allfeatures, JSON_PRETTY_PRINT);
     }
}