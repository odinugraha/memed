<?php
namespace Admin\Master;
use BasicController;
use DB;
use Lang;
use Input;

class KabcomboController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new Kabupaten();
     }
     public function index(){
          $param=Input::all();
          $param['term']=!empty($param['term'])? $param['term'] :'';
          $param['kode']=!empty($param['kode'])? $param['kode'] :'';
          $param['idprov']=!empty($param['idprov'])? $param['idprov'] :'';

           try {
                $query = DB::table($this->model->getTable())
                        ->select('kabId as id','kabId as kode','kabNama as nama','kabNama as text')
                        ->where('kabNama','like','%'.$param['term'].'%')
                        ->where('kabId','like','%'.$param['kode'].'%')
                        ->get();
                
               return $query;                
           }catch(Exception $e){
               return Response::exception($e);
           }

     }
}