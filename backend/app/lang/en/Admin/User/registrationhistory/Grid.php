<?php
/*
 * Langguage mapping for Role
 */
$data['kolom']= array(
                    array(
                        "title"=>"KODE TRANSAKSI", 
                        "data"=>"regCode"
                        ),
                    array(
                        "title"=>"Tanggal/Jam", 
                        "data"=>"regCreateTime"
                        ),
                    array(
                        "title"=>"Rumah Sakit", 
                        "data"=>"mshospitalName"
                        ),
                    array(
                        "title"=>"Poli", 
                        "data"=>"mspolyName"
                        ),
                    array(
                        "title"=>"Dokter", 
                        "data"=>"doc_schName"
                        ),
                    array(
                        "title"=>"Status", 
                        "data"=>"status"
                        ),      
                );
return $data;