<?php
/**
 * Base controller 
 * 
 * Validation
 * @see https://github.com/fadion/ValidatorAssistant
 */
namespace Agape\Controller;

use DB;
use Response;
use Input;
use Controller;
use Exception;
use Paginator;
use Schema;
use Session;
use \Agape\Auth\Permission as Permit;

class BasicController extends Controller {
        
        /**
         * Repository model object
         * @var type Object Model
         */
        protected $model;
        
        /**
         * Validator object
         * This object of validator class
         * @var type Object Validator
         */
        protected $validator = null;
        
        /**
         * Validation result object. 
         * This contains result after validate input data.
         * @var type Object Validator
         */
        protected $validation = null;
        
        /**
         * Menu Link
         * This contains menuLink of each module
         * @var string menuLink 
         */
        protected $menuLink = null;
        
        /**
     * Display a listing of the resource.
         * The default list is undeleted list
     * GET /admin/master/base
     *
     * @return Response
     */
    public function index()
    {
            try {
               
                if(!Permit::view($this->menuLink)){
                    return Response::message(144); 
                }
                
                $query = DB::table($this->model->getTable());
                return $this->getDataGrid($query);                
            }catch(Exception $e){
                return Response::exception($e);
            }    
    }

    /**
     * Store a newly created resource in storage.
     * POST /admin/master/base
     * 
     * @return Response
     */
    public function store()
    {
//            try {
                if(!Permit::store($this->menuLink)){
                    return Response::message(144); 
                }
                
                if (method_exists($this, 'beforeStore'))
                {
                    $this->beforeStore();
                }

                if($this->validator){
                    $isValid = $this->isValidStoreData();

                    // return error if input data is not valid.
                    if(!$isValid){
                        $error = $this->validation->messages()->toArray();
                        $result = Response::message(107, $error);
                        return Response::json($result); 
                    }
                    $input = $this->validation->inputs();
                } else {
                    $input = Input::all();
                }

                if($this->model->usesCompanyStamps()){
                    $input[$this->model->getCompanyField()] = Session::get('companyId');
                }
        
                $query = $this->model->create($input);
                $id = '';
                
                if($query){
                    $id = $query->{$this->model->getKeyName()};
                    
                    if (method_exists($this, 'afterStoreSuccess'))
                    {
                        $this->afterStoreSuccess($id);
                    }
                    
                    $result = Response::message(5);
                    $result['data'] = $this->model->find($id)->toArray();
                    $result['data']['id'] = $id;
                } else {
                    $result = Response::message(107);
                    $result['data'] = null;
                }
                
                return Response::json($result); 
//            }catch(Exception $e){
//                $error = Response::message(107);
//                $error['status']['exception'] = $e;
//                return $error;
//            } 
        }

        /**
     * Display the specified resource.
     * GET /admin/master/base/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        try {
                if(!Permit::view($this->menuLink)){
                    return Response::message(144); 
                }
                
                $result = array();
                $id = $this->checkId($id);
                $data = $this->getShowData($id);
                
                // data not found
                if($data == null){
                    $result = Response::message(103);
                    $result['data'] = null;
                } else {
                    $result['data'] = $data;
                } 
                
                $result['permission'] = Permit::all($this->menuLink);
                return Response::json($result);
            }catch(Exception $e){
                return Response::exception($e);
            } 
    }
        
        
        /**
     * Display the specified resource.
     * GET /admin/master/base/{id}
     *
     * @param  int  $id
     * @return Response
     */
        protected function getShowData($id) {
            $data = $this->model->find($id);
            if($data){
                $data = $data->toArray();
            }
            return $data;
        }

    /**
     * Update the specified resource in storage.
     * PUT /admin/master/base/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
//            try {
                
                if(!Permit::update($this->menuLink)){
                    return Response::message(144); 
                }
            
                if (method_exists($this, 'beforeUpdate'))
                {
                    $this->beforeUpdate();
                }
                
                // check existency
                $data = $this->model->find($id);
                if($data == null){
                    $result = Response::message(103);
                    $result['data'] = null;
                    return Response::json($result); 
                }
                
                if($this->validator){
                    // check validation
                    $isValid = $this->isValidStoreData();
                    if(!$isValid){
                        $error = $this->validation->messages()->toArray();
                        $result = Response::message(107, $error);
                        return Response::json($result); 
                    }
                    $input = $this->validation->inputs();
                } else {
                    $input = Input::all();
                }
                
                if($data == null){
                    $result['data'] = null;
                } else {
                    if($this->model->usesCompanyStamps()){
                        $input[$this->model->getCompanyField()] = Session::get('companyId');
                    }
                    
                    $query = $data->update($input);
                    
                    if (method_exists($this, 'afterUpdate'))
                    {
                        $this->afterUpdate($id);
                    }
                    
                    if($query){
                        
                        if (method_exists($this, 'afterUpdateSuccess'))
                        {
                            $this->afterUpdateSuccess($id);
                        }

                        $result = Response::message(3);
                        $result['data'] = $this->model->find($id)->toArray();
                        $result['data']['id'] = $id;
                    } else {
                        $result = Response::message(106);
                        $result['data'] = null;
                    }
                }
                
                return Response::json($result);
//            }catch(Exception $e){
//                $error = Response::message(106);
//                $error['status']['exception'] = $e;
//                return $error;
//            }
    }

    /**
     * Remove the specified resource from storage.
         * Soft Delete. Mark delete_at and deleted_by column
     * DELETE /admin/master/base/{id}
     *
     * @param  int  $param parameter Id can be id collective (1,2,3) or single id (1)
     * @return Response
     */
    public function destroy($param)
    {
        try {
                if(!Permit::delete($this->menuLink)){
                    return Response::message(144); 
                }
                
                
                if (method_exists($this, 'beforeDestroy'))
                {
                    $this->beforeDestroy();
                }


                $id = $this->checkId($param);
                $data = $this->model->where($this->model->getKeyName() , '=', $id);                
                
                // data not found
                if($data == null){
                    $result = Response::message(103);
                } else {
                    $this->model->setAttribute($this->model->getKeyName(), $id);
                    $status = $this->model->runSoftDelete();
                    $result = Response::message($status);
                } 
                
                $result['permission'] = Permit::all($this->menuLink);
                
                return Response::json($result);
            }catch(Exception $e){
                $error = Response::message(138);
                $error['status']['exception'] = $e;
                return $error;
            }
    }
        
        /**
         * Retrive default error message when delete data
         * 
         * @param boolean $isSuccess
         * @return Response
         */
        public function getDestroyMessage($isSuccess = true)
    {
            if($isSuccess){
                $result = Response::message(2);
            } else {
                $result = Response::message(138);
            }
            $result['permission'] = Permit::delete($this->menuLink);
            return Response::json($result); 
        }
        
        /**
     * Get data grid based on specific query.
         * This uses for displaying data grid with customized query.
     * Developer can customized index() query then send to getDataGrid to 
         * get default return values.
         * 
         * @param DB $query
     * @return Response
     */
        protected function getDataGrid($query)
    {
//            try {
            
                if(!Permit::view($this->menuLink)){
                    return Response::message(144); 
                }
                
                if($this->model->usesCompanyStamps()){
                    $query = $query->where($this->model->getCompanyField(),'=', Session::get('companyId'));
                }
                $query = $this->showActiveData($query);                
                $query = $this->filters($query);                
                $query = $this->sorting($query);
                $query = $this->paging($query);
                $query['permission'] = Permit::all($this->menuLink);
                
                return Response::json($query);
//            }catch(Exception $e){
//                return Response::exception($e);
//            }
        }
        /**
         * Filter grid data result
         * 
         * @param DB $query
         * @return DB
         */
    protected function showActiveData($query)
    {
        try {
                $deleted_by = $this->model->getDeletedBy();
                if (Schema::hasColumn($this->model->getTable(), $deleted_by))
                {
                    $query = $query->whereNull($deleted_by);
                }
                return $query;
            }catch(Exception $e){
                echo Response::exception($e);
                return $query;
            }
    }
        
        /**
         * Filter grid data result
         * 
         * @param DB $query
         * @return DB
         */
    protected function filters($query)
    {
        try {
                if(Input::get('searchCol') && Input::get('searchText')){
                    switch(Input::get('searchType')){
                        case 1: 
                            $query = $query->where(Input::get('searchCol'), Input::get('searchText'));
                            break;
                        case 2: 
                            $query = $query->where(Input::get('searchCol'), 'LIKE', '%'.Input::get('searchText').'%');
                            break;
                        case 3: 
                            $query = $query->where(Input::get('searchCol'), 'LIKE', Input::get('searchText').'%');
                            break;
                        case 4: 
                            $query = $query->whereNotIn(Input::get('searchCol'), Input::get('searchText'));
                            break;
                    }                    
                }
                return $query;
            }catch(Exception $e){
                echo Response::exception($e);
                return $query;
            }
    }
        
        /**
         * Order by Queries
         * 
         * @param DB $query
         * @return DB
         */
    protected function sorting($query)
    {
        try {
                if(Input::get('order') > 0 && Input::get('sort')){
                    if(Input::get('order') == '2'){
                        $order = 'desc';
                    } else {
                        $order = 'asc';
                    }
                    $query = $query->orderBy(Input::get('sort'), $order);                
                }
                return $query;
            }catch(Exception $e){
                echo Response::exception($e);
                return $query;
            }
    }

        /**
         * Set query for paging the result
         * 
         * @param DB $query
         * @return DB
         */
    protected function paging($query)
    {

                $param=Input::all();

                $param['start']=!empty($param['start'])?$param['start']:0;
                $param['draw']=!empty($param['draw'])?$param['draw']:1;

                $start=$param['start']+10;
                $hal  =ceil($start/10);
                $draw =$param['draw'];

                if(Input::get('limit') == -1){
                    return array( 'data' => $query->get());
                }
                
                if($this->model->getPerPage() == -1){
                    return array( 'data' => $query->get());
                }
                
                //if(Input::get('page')){
                //    $page = Input::get('page');
                if($hal){
                    $page = $hal;
                } else {
                    $page = 1;
                }
                Paginator::setCurrentPage($page);
                
                if(Input::get('limit') > 0){
                    $limit = Input::get('limit');
                } else {
                    $limit = $this->model->getPerPage();
                }
                
                $result = $query->paginate($limit);
                return $this->model->paginateToArray($result,$draw);
    }
        
        /**
         * Convert Id into array if it contains ','
         * 
         * @param Obj $id
         * @return Obj
         */
        protected function checkId($id){
            return (strpos($id, ',') !== FALSE)? explode(',', $id):$id;
        }
        
        /**
         * Validate store data
         * 
         * @param Obj $id
         * @return Obj
         */
        protected function isValidStoreData(){
            if(!is_object($this->validator)) {
                return true;
            }
            
            $this->validation = $this->validator->make(Input::all());

            if ($this->validation->fails())
            {
                return false;
            }
            return true;
        }
        
        /**
        * Get request payload values
        * @return array
        */
       public function getRequestPayload() {
           $input = array();
           $json = (array) Input::json();
           if(!empty($json)){
               foreach ($json as $key => $values) {
                   $input = $values;
                   break;
               }
           }
           return $input;
       }

       public function dateName($tgl){
            $a=explode("/",$tgl);
            if($a[0]=='01'){
                $bln="Januari";
            }elseif($a[0]=='02'){
                $bln="Februari";                
            }elseif($a[0]=='03'){
                $bln="Maret";                
            }elseif($a[0]=='04'){
                $bln="April";                
            }elseif($a[0]=='05'){
                $bln="Mei";                
            }elseif($a[0]=='06'){
                $bln="Juni";                
            }elseif($a[0]=='07'){
                $bln="Juli";                
            }elseif($a[0]=='08'){
                $bln="Agustus";                
            }elseif($a[0]=='09'){
                $bln="September";                
            }elseif($a[0]=='10'){
                $bln="Oktober";                
            }elseif($a[0]=='11'){
                $bln="November";                
            }elseif($a[0]=='12'){
                $bln="Desember";                
            }

            return $a[1].' '.$bln.' '.$a[2];
       }


        public function tambahNol($tgl){
            $a=explode('-', $tgl);

            if(strlen($a[1])==1){
                $bln='0'.$a[1];
            }else{
                $bln=$a[1];            
            }

            if(strlen($a[2])==1){
                $tgl='0'.$a[2];
            }else{
                $tgl=$a[2];            
            }

            return $a[0].'-'.$bln.'-'.$tgl;

        }

        public function parse_string($string, $pjcell)
        {   
            $string_parse = explode(" ",trim($string));
            $jumlah_parse = count($string_parse);
            $x=0;
            $str[$x]='';
            for($a=0;$a<=$jumlah_parse-1;$a++)
            {

                if(strlen($string_parse[$a])+strlen($str[$x]) > $pjcell)
                {
                    $x++;
                    $str[$x]='';
                }
                $str[$x] = $str[$x].$string_parse[$a]." ";

            }
            return $str;
        }

       public function getAnggaranKeg($kdskpd,$kdkeg){
            $query = DB::table('angdrka')
                     ->select(
                                DB::raw('sum(adrkaNilai) as adrkaNilai'),
                                DB::raw('sum(adrkaNilaiUbah) as adrkaNilaiUbah'),
                                DB::raw('sum(adrkaRevisi1) as adrkaRevisi1'),
                                DB::raw('sum(adrkaRevisi2) as adrkaRevisi2'),
                                DB::raw('sum(adrkaRevisi3) as adrkaRevisi3')
                              )
                     ->where('adrkaSkpdKd','=',$kdskpd)
                     ->where('adrkaKegKd','=',$kdkeg)
                     ->get();
            
            $res['adrkaNilai']=0;
            $res['adrkaNilaiUbah']=0;
            $res['adrkaRevisi1']=0;
            $res['adrkaRevisi2']=0;
            $res['adrkaRevisi3']=0;
            foreach ($query as $key => $value) {
                $res['adrkaNilai']=$value->adrkaNilai;
                $res['adrkaNilaiUbah']=$value->adrkaNilaiUbah;
                $res['adrkaRevisi1']=$value->adrkaRevisi1;
                $res['adrkaRevisi2']=$value->adrkaRevisi2;
                $res['adrkaRevisi3']=$value->adrkaRevisi3;
            }         


            $conf = DB::table('company')
                     ->select('compStatusAnggaran')
                     ->get();
            $status=$conf[0]->compStatusAnggaran;

            if($status=="1"){
                return $res['adrkaNilai'];          
            }elseif($status=="2"){
                return $res['adrkaNilaiUbah'];          
            }elseif($status=="3"){
                return $res['adrkaRevisi1'];          
            }elseif($status=="4"){
                return $res['adrkaRevisi2'];          
            }elseif($status=="5"){
                return $res['adrkaRevisi3'];          
            }

       }   


       public function getAnggaranRek($kdskpd,$kdkeg,$kdrek5){
            $query = DB::table('angdrka')
                     ->select($this->fieldAnggaran().' as nilaiAng')
                     ->where('adrkaSkpdKd','=',$kdskpd)
                     ->where('adrkaKegKd','=',$kdkeg)
                     ->where('adrkaRek5Kd','=',$kdrek5)
                     ->get();
            
            $res=0;
            foreach ($query as $key => $value) {
                $res=$value->nilaiAng;
            }         

            return $res;
       }   


     public function fieldAnggaran(){
            $conf = DB::table('company')
                     ->select('compStatusAnggaran')
                     ->get();
            $status=$conf[0]->compStatusAnggaran;

            if($status=="1"){
                return 'adrkaNilai';          
            }elseif($status=="2"){
                return 'adrkaNilaiUbah';          
            }elseif($status=="3"){
                return 'adrkaRevisi1';          
            }elseif($status=="4"){
                return 'adrkaRevisi2';          
            }elseif($status=="5"){
                return 'adrkaRevisi3';          
            }
     }  


     public function getSumSpdLalu($kdskpd,$kdkeg,$nospd){

            $query = DB::table('angspddetail')
                     ->select(DB::raw('sum(spdDetNilai) as nilai'))
                     ->where('spdDetSkpd','=',$kdskpd)
                     ->where('spdDetKeg','=',$kdkeg)
                     ->where('spdDetNo','<>',$nospd)
                     ->get();
            
            $res=0;
            foreach ($query as $key => $value) {
                $res=$value->nilai;
            }         

            return $res;
     }


   public function realisasiSetoran($skpd,$keg,$rek){

            $query = DB::table('tukdterimadetail')
                    ->select(DB::raw('sum(trmDetNilai) as realNilai'))
                    ->where('trmDetSkpdKd','=',$skpd)
                    ->where('trmDetKegKd','=',$keg)
                    ->where('trmDetRek5Kd','=',$rek)
                    ->get()
                    ;
            $res=0;

            foreach ($query as $key => $value) {
                $res=$value->realNilai;
            }

            return $res;
   }  


     public function getAnggaranRekSPD($kdskpd,$kdkeg,$kdrek5){
          $query1 = DB::table('angdrka')
                   ->select($this->fieldAnggaran().' as nilaiAng')
                   ->where('adrkaSkpdKd','=',$kdskpd)
                   ->where('adrkaKegKd','=',$kdkeg)
                   ->where('adrkaRek5Kd','=',$kdrek5)
                   ->get();
          
          $nrek5=0;
          foreach ($query1 as $key => $value) {
              $nrek5=$value->nilaiAng;
          }         


          $query2 = DB::table('angdrka')
                   ->select(DB::raw('sum('.$this->fieldAnggaran().') as nilaiAng'))
                   ->where('adrkaSkpdKd','=',$kdskpd)
                   ->where('adrkaKegKd','=',$kdkeg)
                   ->get();
          
          $nkeg=1;
          foreach ($query2 as $key => $value) {
              $nkeg=$value->nilaiAng;
          }         

          if($nkeg==0) $nkeg=1;

          $persen=($nrek5/$nkeg);


          $query3 = DB::table('angspddetail')
                   ->select(DB::raw('sum(spdDetNilai*'.$persen.') as nilaiAng'))
                   ->where('spdDetSkpd','=',$kdskpd)
                   ->where('spdDetKeg','=',$kdkeg)
                   ->get();
          
          $nspd=0;
          foreach ($query3 as $key => $value) {
              $nspd=$value->nilaiAng;
          }         

          return $nspd;
     }   


     public function getRealRekSPP($kdskpd,$kdkeg,$kdrek5){
      
          $query1 = DB::table('tukdkeluardetail')
                   ->select(DB::raw('sum(klrDetNilai) as nilaiReal'))
                   ->where('klrDetSkpdKd','=',$kdskpd)
                   ->where('klrDetKegKd','=',$kdkeg)
                   ->where('klrDetRek5Kd','=',$kdrek5)
                   ->get();
          
          $nreal=0;
          foreach ($query1 as $key => $value) {
              $nreal=$value->nilaiReal;
          }         

          return $nreal;
     }


     public function getRealSpjLalu($kdskpd,$kdkeg,$kdrek5){
      
          $query1 = DB::table('tukdspjdetail')
                   ->select(DB::raw('sum(klrDetNilai) as nilaiReal'))
                   ->where('klrDetSkpdKd','=',$kdskpd)
                   ->where('klrDetKegKd','=',$kdkeg)
                   ->where('klrDetRek5Kd','=',$kdrek5)
                   ->get();
          
          $nreal=0;
          foreach ($query1 as $key => $value) {
              $nreal=$value->nilaiReal;
          }         

          return $nreal;
     }

     public function getRealBendaharaLalu($kdskpd,$kdkeg,$kdrek5){
      
          $query1 = DB::table('tukdbendaharadetail')
                   ->select(DB::raw('sum(klrDetNilai) as nilaiReal'))
                   ->where('klrDetSkpdKd','=',$kdskpd)
                   ->where('klrDetKegKd','=',$kdkeg)
                   ->where('klrDetRek5Kd','=',$kdrek5)
                   ->get();
          
          $nreal=0;
          foreach ($query1 as $key => $value) {
              $nreal=$value->nilaiReal;
          }         

          return $nreal;
     }


     public function reversdate($tanggal){
        if(substr($tanggal, 2,1)=="/" or substr($tanggal, 1,1)=="/"){
            $a=explode("/",$tanggal);
            $result=$a[2].'-'.$a[0].'-'.$a[1];
        }elseif(substr($tanggal, 2,1)=="-" or substr($tanggal, 1,1)=="-"){
            $a=explode("-",$tanggal);
            $result=$a[2].'-'.$a[1].'-'.$a[0];
        }else{
            $result=$tanggal; 
        }
        return $result;
     }



}