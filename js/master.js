
function tableMaster(modulx){
    $.ajax({
        type: "GET",
        url: "backend/public/api/admin/config/getapimenu",
        data:({modul:modulx}),
        dataType:"json",
        success:function(data){
            getLangGrid(data[0].apiLangGrid,data[0].apiData,data[0].apiLangForm);
            getLangForm(data[0].apiLangForm);
            
        }
    });      
}

function getLangGrid(apiLang,apiData,apiLangForm){
    $.ajax({
        type: "GET",
        url: apiLang,
        dataType:"json",
        success:function(data){
            setGrid(data.kolom,apiData,apiLangForm);
        }
    });          
}

function setGrid(kolom,apiData,apiLangForm){

	// var table=$('#tData').DataTable({
	// 	"sorting": [[ 0, "asc" ]],
	// 	"paginationType": "bootstrap",
	// 	"paging":   true,		
 //        "info"  :   true,
 //        "language": {
	// 		            "lengthMenu": "",
	// 		            "zeroRecords": "No records available",
	// 		            "info": "Page _PAGE_ of _PAGES_",
	// 		            "infoEmpty": "No records available",
	// 		            "infoFiltered": "(filtered from _MAX_ total records)",
	// 		            "search": "SEARCH :  ",
	// 		            "paginate": {"next": "","previous": ""}
 //        },
 //        "processing": false,
 //        "serverSide": true,
 //        "ajax": {
 //        			"url" : apiData,
 //        			"dataType" : "json",
	// 	        },
				        
 //        "columns": kolom,    
	// });

    var table=$('#tData').DataTable( {
        "processing": false,
        "serverSide": true,
        "searching": true,
        "ajax": {
                    "url" : apiData,
                    "dataType" : "json",
                },
        "columns": kolom,
    } );


    $('#tData tbody').on('click', 'tr', function () {
        // var data = $(this).parents('tr').context.cells;
        var idData = $(this).find('td:eq(0)').text();
        setFormData(apiData,idData,apiLangForm);
    })

}

function setFormData(apiData,idData,apiLangForm){

    $.ajax({
        type: "GET",
        url: apiData+'/'+idData,
        dataType:"json",
        success:function(data){
            var isi=data.data;
            var key=Object.keys(isi);

           // console.log(key[0]);

            $.ajax({
                type: "GET",
                url: apiLangForm,
                dataType:"json",
                success:function(data2){
                       // console.log(data2);    

                    for (i = 0; i <= data2.form.length-1; i++) {


                        var nmfield=data2.form[i].id;
                        var iData='isi.'+nmfield;


                        if(data2.form[i].type=='text'){
                            document.getElementById(nmfield).value=eval(iData);
                        }else if(data2.form[i].type=='angka'){
                            document.getElementById(nmfield).value=eval(iData);                            
                        }else if(data2.form[i].type=='hidden'){
                            document.getElementById(nmfield).value=eval(iData);
                        }else if(data2.form[i].type=='textarea'){
                            document.getElementById(nmfield).value=eval(iData);
                        }else if(data2.form[i].type=='combo'){
                            $("#"+nmfield).val(eval(iData));
                        }else if(data2.form[i].type=='autocomplete'){
                            var kodecombo=eval(iData);    
                            var comboapi=data2.form[i].comboapi;
                            setAutocompleteVal(comboapi,kodecombo,nmfield);
                        }else if(data2.form[i].type=='date'){
                            var dataxx=eval(iData);    
                            var res = dataxx.split("-");
                            var hasil = res[1]+'/'+res[2]+'/'+res[0];
                            // $("#"+nmfield).attr('value',hasil);
                            document.getElementById(nmfield).value=hasil;
                        }    

                    }
                }
            });          
        }
    });          
}


function setAutocompleteVal(comboapi,kodecombo,nmfield){
    $.ajax({
        type: "GET",
        url: comboapi,
        data:({kode:kodecombo}),
        dataType:"json",
        success:function(data3){
            $("#"+nmfield).select2("data", { id: kodecombo, text:data3[0].text});
        }
    });    
}


function getLangForm(apiLang){
    $.ajax({
        type: "GET",
        url: apiLang,
        dataType:"json",
        success:function(data){
            //console.log(data);
            var result="";
            for (i = 0; i <= data.form.length-1; i++) {

                if(data.form[i].readonly=='1'){
                    var ro='readonly';
                }else{
                    var ro='';
                }


                if(data.form[i].type=='file'){
                    result +='<div class="form-group row">';
                    result +='     <label for="exampleInputEmail2" class="col-sm-3 col-form-label">'+data.form[i].name+'</label>';
                    result +='     <input type="file" class="form-control" name="'+data.form[i].id+'" id="'+data.form[i].id+'" '+ro+' />';
                    result +='</div>';
                }


                if(data.form[i].type=='date'){
                    result +='<div class="form-group row">';
                    result +='     <label for="exampleInputEmail2" class="col-sm-3 col-form-label">'+data.form[i].name+'</label>';
                    result +='     <input type="text" class="form-control" name="'+data.form[i].id+'" id="'+data.form[i].id+'" '+ro+' placeholder="Pilih Tanggal" />';
                    result +='</div>';
                }


                if(data.form[i].type=='hidden'){
                    result +='<div class="form-group row">';
                    result +='     <input type="hidden" class="form-control" name="'+data.form[i].id+'" id="'+data.form[i].id+'" />';
                    result +='</div>';
                }
                
                if(data.form[i].type=='text'){
                    result +='<div class="form-group row">';
                    result +='  <label for="exampleInputEmail2" class="col-sm-3 col-form-label">'+data.form[i].name+'</label>';
                    result +='  <div class="col-sm-9">';
                    result +='    <input type="text" class="form-control" name="'+data.form[i].id+'" id="'+data.form[i].id+'" '+ro+' placeholder="'+data.form[i].name+'">';
                    result +='  </div>';
                    result +='</div>';
                }


                if(data.form[i].type=='angka'){
                    result +='<div class="form-group row">';
                    result +='     <label for="exampleInputEmail2" class="col-sm-3 col-form-label">'+data.form[i].name+'</label>';
                    result +='     <input type="text" class="form-control" style="text-align:right" name="'+data.form[i].id+'" id="'+data.form[i].id+'"  '+ro+' />';
                    result +='</div>';

                }


                if(data.form[i].type=='textarea'){
                    result +='<div class="form-group row">';
                    result +='     <label for="exampleInputEmail2" class="col-sm-3 col-form-label">'+data.form[i].name+'</label>';
                    result +='     <textarea class="form-control" name="'+data.form[i].id+'" id="'+data.form[i].id+'" '+ro+' row="'+data.form[i].row+'" style="height:'+data.form[i].height+'"/></textarea>';
                    result +='</div>';
                }

                if(data.form[i].type=='password'){
                    result +='<div class="form-group row">';
                    result +='     <label for="exampleInputEmail2" class="col-sm-3 col-form-label">'+data.form[i].name+'</label>';
                    result +='  <div class="col-sm-9">';
                    result +='     <input type="password" class="form-control" name="'+data.form[i].id+'" id="'+data.form[i].id+'" '+ro+' placeholder="'+data.form[i].name+'">';
                    result +='</div>';
                    result +='</div>';
                }

                if(data.form[i].type=='combo'){
                    var comboapi=data.form[i].comboapi;

                    result +='<div class="form-group row">';
                    result +='    <label for="exampleInputEmail2" class="col-sm-3 col-form-label">'+data.form[i].name+'</label>';
                    result +='  <div class="col-sm-9">';
                    result +='    <select class="form-control" name="'+data.form[i].id+'" id="'+data.form[i].id+'" '+ro+'>';
                    result +='        <option></option>';

                    $.ajax({
                        type: "GET",
                        url: comboapi,
                        dataType:"json",
                        async: false,
                        success:function(datacombo){
                            for(j=0;j<=datacombo.length-1;j++){
                                result +='<option value="'+datacombo[j].kode+'">'+datacombo[j].nama+'</option>';
                            }
                        }    
                    });    

                    result +='    </select>';
                    result +='</div>';
                    result +='</div>';
                }


                if(data.form[i].type=='autocomplete'){
                    var comboapi=data.form[i].comboapi;

                    result +='<div class="form-group row">';
                    result +='    <label for="exampleInputEmail2" class="col-sm-3 col-form-label">'+data.form[i].name+'</label>';
                    result +='  <div class="col-sm-9">';
                    result +='    <input type="hidden" class="populate placeholder" name="'+data.form[i].id+'" id="'+data.form[i].id+'" >';
                    result +='    <input type="hidden" name="'+data.form[i].id+'_hidden" id="'+data.form[i].id+'_hidden" >';
                    result +='</div>';
                    result +='</div>';

                }

            }

            $('#forminput').html(result);


            for (i = 0; i <= data.form.length-1; i++) {


                if(data.form[i].type=='angka'){
                    $('#'+data.form[i].id).keypress(function(){
                      return(numbersonly(event));
                    });
                }    


                if(data.form[i].type=='autocomplete'){
                    var comboapi=data.form[i].comboapi;

                    $('#'+data.form[i].id)
                        .on("change", function(e) {
                              var id=e.added.id;
                              var txt=e.added.nama;  
                              var nmhidden=this.id+'_hidden';
                              //console.log(nmhidden);                              
                              $('#'+nmhidden).attr('value',txt);
                        })      
                        .select2({
                        theme: 'flat',
                        width: 'style',
                        placeholder: "Pilih Data",
                        allowClear: Boolean($(this).data('allow-clear')), 
                        ajax: {
                                url: comboapi,
                                dataType: 'json',
                                quietMillis: 100,
                                data: function (term, page) {
                                    return {
                                        term: term, //search term
                                        page_limit: 10 // page size
                                    };
                                },
                                results: function (data, page) {
                                    return { results: data };
                                },
                            }
                    });

                }  

                if(data.form[i].type=='date'){
                    $('#'+data.form[i].id).datepicker({
                        format: 'dd/mm/yyyy',
                    });                
                }                 
            }




        }
    });              
}


function newdata(){
    var modulx=document.getElementById('modul').value;

    $.ajax({
        type: "GET",
        url: "backend/public/api/admin/config/getapimenu",
        data:({modul:modulx}),
        dataType:"json",
        async: false,
        success:function(data){
            var apiForm=data[0].apiLangForm;
            var apiGrid=data[0].apiLangGrid;
            var apiData=data[0].apiData;    

            $.ajax({
                type: "GET",
                url: apiForm,
                dataType:"json",
                success:function(data2){
                    var fElement=data2.form;

                    for (i = 0; i <= fElement.length-1; i++) {

                        if(fElement[i].type=='autocomplete'){
                            $("#"+fElement[i].id).select2("data", { id: '', text:''});
                        }else{
                            document.getElementById(fElement[i].id).value='';                            
                        }
    
                    }

                }                
            });               
        }
    });      
}

function deleteData(){


    var modulx=document.getElementById('modul').value;
    $.ajax({
        type: "GET",
        url: "backend/public/api/admin/config/getapimenu",
        data:({modul:modulx}),
        dataType:"json",
        async: false,
        success:function(data){
            var apiForm=data[0].apiLangForm;
            var apiGrid=data[0].apiLangGrid;
            var apiData=data[0].apiData;    


            $.ajax({
                type: "GET",
                url: apiForm,
                dataType:"json",
                success:function(data2){
                    //console.log(data2);
                    var postData=new Object();
                    var fElement=data2.form;
                    var idData =document.getElementById(fElement[0].id).value 

                    if(idData==''){
                        alert('Data Belum Dipilih !!!');
                    }else{
                        var method='DELETE';
                        var apiUrl=apiData+'/'+idData;

                        if(confirm("Anda Akan Menghapus Data Ini?")){
                            $.ajax({
                                type: method,
                                url: apiUrl,
                                dataType:"json",
                                data:postData,
                                success:function(data3){

                                    newdata();
                                    var oTableToUpdate =  $('#tData').dataTable( { bRetrieve : true } );
                                        oTableToUpdate .fnDraw();                            
                                    //console.log(data3);
                                }
                            });
                        }
                    }


                }                
            });    

           
        }
    });      

}

function save(){

    var user='';
    $.ajax({
        type: "GET",
        url: "backend/public/getSession",
        dataType:"json",
        async:false,
        success:function(data){            
            user=data.userName;
        }
    });          

    var modulx=document.getElementById('modul').value;
    $.ajax({
        type: "GET",
        url: "backend/public/api/admin/config/getapimenu",
        data:({modul:modulx}),
        dataType:"json",
        async: false,
        success:function(data){
            var apiForm=data[0].apiLangForm;
            var apiGrid=data[0].apiLangGrid;
            var apiData=data[0].apiData; 
            //alert('ini Api data '+ apiData);

            $.ajax({
                type: "GET",
                url: apiForm,
                dataType:"json",
                success:function(data2){
                    //console.log(data2);
                    var postData=new Object();
                    var fElement=data2.form;
                    var idData =document.getElementById(fElement[0].id).value 

                    for (i = 0; i <= fElement.length-1; i++) {
                        if(fElement[i].type!='file'){
                            postData[fElement[i].id]=document.getElementById(fElement[i].id).value;
                            //alert(document.getElementById(fElement[i].id).value);
                        }

                        if(fElement[i].type=='autocomplete'){
                            postData[fElement[i].id+'_hidden']=document.getElementById(fElement[i].id+'_hidden').value; 
                            //alert(document.getElementById(fElement[i].id+'_hidden').value);
                        }
                    }

                    if(idData==''){
                        var method='POST';
                        var apiUrl=apiData;
                    }else{
                        var method='PUT';
                        var apiUrl=apiData+'/'+idData;
                    }

                    $.ajax({
                        type: method,
                        url: apiUrl,
                        dataType:"json",
                        data:postData,
                        success:function(data3){

                            idimg=data3.data.id;

                            for (i = 0; i <= fElement.length-1; i++) {

                                if(fElement[i].type=='file'){
                                    var imgfile = document.getElementById(fElement[i].id);  
                                    formdata = new FormData(); 
                                    formdata.append(fElement[i].id,imgfile.files[0]);
                                    formdata.append(fElement[0].id,idimg);

                                    $.ajax({
                                        type: 'POST',
                                        url: 'backend/public/api/admin/setup/upload',
                                        dataType:"json",
                                        async: false,
                                        data:formdata,
                                        processData: false,  
                                        contentType: false,  
                                        success:function(data4){
                                            console.log(data4);
                                        }
                                    });                                        
                                }
                            }

                            // if(idData==''){
                            //     postData['user']=user;
                            //     $.ajax({
                            //         type: 'POST',
                            //         url: 'rsudwsapi/simpan_master_obat.php',
                            //         dataType:"json",
                            //         async: false,
                            //         data:postData,
                            //         success:function(simrs){
                            //             console.log(simrs);
                            //             // if(simrs.kode==404){
                            //             //     alert(simrs.pesan);                                            
                            //             // }
                            //         }
                            //     });                                      
                            // }

                            newdata();
                            var oTableToUpdate =  $('#tData').dataTable( { bRetrieve : true } );
                                oTableToUpdate .fnDraw();                            
                                //console.log(data3);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
                            alert(textStatus+" : " + errorThrown+" -> Kode Sudah Terpakai"); 
                        } 
                    });
                }                
            });    

           
        }
    });      
}


function cetakProfil(id){
    var param="?id="+id;

    if(id==''){
        return;
    }

    var url="backend/public/api/admin/laporan/cetakprofil";                    
    window.open(url+param, '_blank');
}

function printData(id){
    var param="?id="+id;
    var url="report/cetakkasir.php";                    
    window.open(url+param, '_blank');    
}

function printData2(id){
    var param="?id="+id;
    var url="report/cetakkwitansi.php";                    
    window.open(url+param, '_blank');    
}

function getLoc(){
    $.ajax({
        type: "GET",
        url : "backend/public/api/admin/master/location",
        dataType:"json",
        success:function(features){
            console.log(features)
            // var no = data.data.length-1;
            // var coordinates='';
            // for(i=0;i<=no;i++){
            //     lat=data.data[i].mshospitalLat;
            //     long=data.data[i].mshospitalLong;
            //     coordinates=[long,lat];
            // }
            // // var geojson={}
            geoMap(features);
            
        }    
    });          
}


function geoMap(json){
        var geojson = json;
        var saved_markers = [113.69247563604586,-8.179982315245894];
        var user_location = [113.668076,-8.184486];
        mapboxgl.accessToken = 'pk.eyJ1Ijoia29kb2tuZ29yZWsiLCJhIjoiY2p5ZW5iYTVrMTNqMDNocGJjZTAxc3cwNSJ9.WTIWmk6-n6B9ziieoUV8Nw';
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v9',
            center: user_location,
            zoom: 12
        });
        var geocoder = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            marker: true,
            mapboxgl: mapboxgl
        });

        var marker ;
        map.on('load', function() {
            addMarker(user_location,'load');
            add_markers(saved_markers);

            geocoder.on('result', function(ev) {
                alert("aaaaa");
                console.log(ev.result.center);

            });
        });
        map.on('click', function (e) {
            marker.remove();
            addMarker(e.lngLat,'click');
            document.getElementById("mshospitalLat").value = e.lngLat.lat;
            document.getElementById("mshospitalLong").value = e.lngLat.lng;

        });

        function addMarker(ltlng,event) {

            if(event === 'click'){
                user_location = ltlng;
            }
            marker = new mapboxgl.Marker({draggable: true,color:"#d02922"})
                .setLngLat(user_location)
                .addTo(map)
                .on('dragend', onDragEnd);
        }
        function add_markers(coordinates) {

            geojson.features.forEach(function(marker) {
                // create a DOM element for the marker
                var el = document.createElement('div');
                    el.className = 'marker';
                    // el.style.backgroundImage = 'url(src/images/locmemed.png/' + marker.properties.iconSize.join('/') + '/)';
                    // el.style.width = marker.properties.iconSize[0] + 'px';
                    // el.style.height = marker.properties.iconSize[1] + 'px';
                 
                    el.addEventListener('click', function() {
                    window.alert(marker.properties.message);
                });
                 
                // add marker to map
                new mapboxgl.Marker(el)
                    .setLngLat(marker.geometry.coordinates)
                    .addTo(map);
                });

        }

        function onDragEnd() {
            var lngLat = marker.getLngLat();
            document.getElementById("lat").value = lngLat.lat;
            document.getElementById("lng").value = lngLat.lng;
            console.log('lng: ' + lngLat.lng + '<br />lat: ' + lngLat.lat);
        }

        document.getElementById('geocoder').appendChild(geocoder.onAdd(map));
}
