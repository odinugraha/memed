<?php
namespace Admin\Master;

use BasicModels;

class Useradmin extends BasicModels {

  protected $table='admin_users';

        /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
    protected $fillable=array('ausrUsername','ausrPassword','ausrName','ausrRolhId','ausrUnit');
        
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
    protected $primaryKey = 'admin_users';

    public $timestamps = true;
   public $companystamps = false;

}