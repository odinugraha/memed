<?php
/*
 * Langguage mapping for Role
 */
$data['kolom']= array(
                    array(
                        "title"=>"KODE", 
                        "data"=>"doc_schId"
                        ),
                    array(
                        "title"=>"Nama Dokter", 
                        "data"=>"doc_schName"
                        ),
                    array(
                        "title"=>"Hari", 
                        "data"=>"doc_schDay"
                        ),
                    array(
                        "title"=>"Pelayanan", 
                        "data"=>"mshospitalName"
                        ),
                    array(
                        "title"=>"Poli", 
                        "data"=>"mspolyName"
                        ),
                    array(
                        "title"=>"Jam Awal", 
                        "data"=>"doc_schPeriodStart"
                    ),
                    array(
                        "title"=>"Jam Akhir", 
                        "data"=>"doc_schPeriodStop"
                        ),
                     
                );


return $data;