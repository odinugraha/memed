<?php
namespace Admin\Daftar;

use BasicController;
use DB;
use Lang;
use Input;
use Date;
use Auth;
use Session;

class RegistrationController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
        $param=Input::all();
        $this->model = new \Admin\Master\Callingregis();
     }

     public function index(){
           $param=Input::all();
           $id=Session::get('userIdAdmin');

           try {
                $query = DB::table($this->model->getTable())
                        ->select('*')
                        ->where('callregisUserId','=',$id)
                        ->where('callregisStat','=','1')
                        ->orderby('callregisId','asc')
                        ->limit(1);

            $res=$this->getDataGrid($query);   
            return $res;                
                 
           }catch(Exception $e){
               return Response::exception($e);
           }

     }


     public function beforeStore(){
        $param=Input::all();
        $id=Session::get('userIdAdmin');
        $idTrans=$id.'-'.$this->getNoTrans($param['hospital']).'-'.date('Y');

        Input::merge(array( 
                            'callregisCode'=>$idTrans,
                            'callregisUserId'=>$id,
                            'callregisDoctor'=>$param['doctor'],
                            'callregisPolyId'=>$param['poly'],
                            'callregisHospitalId'=>$param['hospital'],
                            'callregisStat'=>$param['stat'],
                          ));                    

     }
     public function getNoTrans($gudang){
            $query = DB::table('trconfnomor')
                    ->select('noCalling')
                    ->limit(1)
                    ->get();            
            
                $nomor=$query[0]->noCalling;
                $nomor ++;
                $q2=DB::table('trconfnomor')
                    ->update(array('noCalling' => $nomor));        
            
            return $this->tambahNol($nomor);
     }

     public function tambahNol($nomor){
        $res=$nomor;
        if(strlen($nomor)==1){
            $res='00000'.$nomor;
        }elseif(strlen($nomor)==2){
            $res='0000'.$nomor;
        }elseif(strlen($nomor)==3){
            $res='000'.$nomor;
        }elseif(strlen($nomor)==4){
            $res='00'.$nomor;
        }elseif(strlen($nomor)==5){
            $res='0'.$nomor;
        }elseif(strlen($nomor)==6){
            $res=$nomor;
        }
        return $res;
     }


     public function reversdate($tanggal){
        if(substr($tanggal, 2,1)=="/" or substr($tanggal, 1,1)=="/"){
            $a=explode("/",$tanggal);
            $result=$a[2].'-'.$a[1].'-'.$a[0];
        }elseif(substr($tanggal, 2,1)=="-" or substr($tanggal, 1,1)=="-"){
            $a=explode("-",$tanggal);
            $result=$a[2].'-'.$a[1].'-'.$a[0];
        }else{
            $result=$tanggal; 
        }
        return $result;
     }


}