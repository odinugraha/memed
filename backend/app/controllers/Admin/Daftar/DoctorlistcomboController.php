<?php
namespace Admin\Daftar;
use BasicController;
use DB;
use Lang;
use Input;

class DoctorlistcomboController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new \Admin\Master\Schedule();
     }
     public function index(){
          $param=Input::all();
          $param['term']=!empty($param['term'])? $param['term'] :'';
          $param['kode']=!empty($param['kode'])? $param['kode'] :'';

           try {
                $query = DB::table($this->model->getTable())
                        ->select('doc_schId as id','doc_schId as kode','doc_schName as nama','doc_schName as text')
                        ->where('doc_schName','like','%'.$param['term'].'%')
                        ->where('doc_schId','like','%'.$param['kode'].'%')
                        ->get();
                
               return $query;                
           }catch(Exception $e){
               return Response::exception($e);
           }

     }
}