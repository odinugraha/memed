<?php
namespace Admin\Master;

use BasicModels;

class Hospital extends BasicModels {

  protected $table='mshospital';

        /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
    protected $fillable=array('mshospitalName','mshospitalAddress','mshospitalLong','mshospitalLat','mstemuanDeskripsi');
        
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
    protected $primaryKey = 'mshospitalId';

    public $timestamps = true;
   public $companystamps = false;
   
   /**
    * The name of the "created at" column.
    *
    * @var string
    */
   const CREATED_AT = 'mshospitalCreateTime';
   
   /**
    * The name of the "created by" column.
    *
    * @var string
    */
   const CREATED_BY = 'mshospitalCreateUser';

   /**
    * The name of the "updated at" column.
    *
    * @var string
    */
   const UPDATED_AT = 'mshospitalUpdateTime';
   
   /**
    * The name of the "updated by" column.
    *
    * @var string
    */
   const UPDATED_BY = 'mshospitalUpdateUser';
   
   /**
    * The name of the "deleted at" column.
    *
    * @var string
    */
   const DELETED_AT = 'mshospitalDeleteTime';
   
   /**
    * The name of the "deleted by" column.
    *
    * @var string
    */
   const DELETED_BY = 'mshospitalDeleteUser';

}