<?php
/**
 * Auth Login
 */
namespace Agape\Auth;

use DB;
use Session;

class Role {
    
    public function authentication()
    {
        $userDb = $this->getRole();
        
        if($userDb['roleNonActive'] != 0){
            return 401; //inactive role
        }
        
        $today = new \DateTime(); // Today
        $todayDate = $today->format('Y-m-d');
        
        switch($today->format('w')){
            case "0" : //sunday
                $active = $userDb['roleSunday'];
                $start = date_parse($userDb['roleSundayStart']);
                $end = date_parse($userDb['roleSundayEnd']);
                break;
            case "1" : //monday
                $active = $userDb['roleMonday'];
                $start = date_parse($userDb['roleMondayStart']);
                $end = date_parse($userDb['roleMondayEnd']);
                break;
            case "2" : //Tuesday
                $active = $userDb['roleTuesday'];
                $start = date_parse($userDb['roleTuesdayStart']);
                $end = date_parse($userDb['roleTuesdayEnd']);
                break;
            case "3" : //Wednesday
                $active = $userDb['roleWednesday'];
                $start = date_parse($userDb['roleWednesdayStart']);
                $end = date_parse($userDb['roleWednesdayEnd']);
                break;
            case "4" : //Thursday
                $active = $userDb['roleThursday'];
                $start = date_parse($userDb['roleThursdayStart']);
                $end = date_parse($userDb['roleThursdayEnd']);
                break;
            case "5" : //Friday
                $active = $userDb['roleFriday'];
                $start = date_parse($userDb['roleFridayStart']);
                $end = date_parse($userDb['roleFridayEnd']);
                break;
            case "6" : //Saturday
                $active = $userDb['roleSaturday'];
                $start = date_parse($userDb['roleSaturdayStart']);
                $end = date_parse($userDb['roleSaturdayEnd']);
                break;
        }
       
        if($active == 0){
            return 402;
        } 
        
        $dateBegin = new \DateTime($todayDate." ".$start['hour'].':'.$start['minute']);
        $dateEnd  = new \DateTime($todayDate." ".$end['hour'].':'.$end['minute']);

        if ($today->getTimestamp() >= $dateBegin->getTimestamp() && 
            $today->getTimestamp() <= $dateEnd->getTimestamp()){
            //
        }else{
           return 402; //role out of allowed time
        }
       
        return 20;
    }
    
   
    public function getRole()
    {   
        if(Session::get('admin')!=''){
        $query = (array) DB::table('role')
                ->where('roleId', '=', DB::raw(Session::get('userRolhIdAdmin')))
                ->first();
        return $query;
        }else if(Session::get('user')!=''){
            $query = (array) DB::table('role')
                ->where('roleId', '=', DB::raw(Session::get('userRolhIdUser')))
                ->first();
        return $query;
        }
    }
}
