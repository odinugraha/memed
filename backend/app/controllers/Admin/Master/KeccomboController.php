<?php
namespace Admin\Master;
use BasicController;
use DB;
use Lang;
use Input;

class KeccomboController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new Kecamatan();
     }
     public function index(){
          $param=Input::all();
          $param['term']=!empty($param['term'])? $param['term'] :'';
          $param['kode']=!empty($param['kode'])? $param['kode'] :'';
          $param['idkab']=!empty($param['idkab'])? $param['idkab'] :'';

           try {
                $query = DB::table($this->model->getTable())
                        ->select('kecId as id','kecId as kode','kecNama as nama','kecNama as text')
                        ->where('kecNama','like','%'.$param['term'].'%')
                        ->where('kecId','like','%'.$param['kode'].'%')
                        ->get();
                
               return $query;                
           }catch(Exception $e){
               return Response::exception($e);
           }

     }
}