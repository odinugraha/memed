<?php
namespace Admin\Master;
use BasicController;
use DB;
use Lang;
use Input;

class KelcomboController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new Kelurahan();
     }
     public function index(){
          $param=Input::all();
          $param['term']=!empty($param['term'])? $param['term'] :'';
          $param['kode']=!empty($param['kode'])? $param['kode'] :'';
          $param['idkec']=!empty($param['idkec'])? $param['idkec'] :'';

           try {
                $query = DB::table($this->model->getTable())
                        ->select('kelId as id','kelId as kode','kelNama as nama','kelNama as text')
                        ->where('kelNama','like','%'.$param['term'].'%')
                        ->where('kelId','like','%'.$param['kode'].'%')
                        ->limit(20)
                        ->get();
                
               return $query;                
           }catch(Exception $e){
               return Response::exception($e);
           }

     }
}