<?php
namespace Admin\Master;

use BasicController;
use DB;
use Lang;
use Input;

class LocationController extends BasicController {
    /**
     * Set Model's Repository
     */
     public function __construct() {
         $this->model = new Hospital();
     }
     public function index()
     {           
      $query = DB::table($this->model->getTable())
              ->select('mshospitalId','mshospitalName','mshospitalLong','mshospitalLat')
              ->get();

      // $result['features']=[]$query;
      $i=0;
      foreach($query as $key => $value) {

            $features[] = array(
                    'type' => 'Feature',
                    'geometry' => array('type' => 'Point', 'coordinates' => array($value->mshospitalLong,$value->mshospitalLat)),
                    'properties' => array('name' => $value->mshospitalName, 'id' => $value->mshospitalId),
                    );
            $i++;
            };  
      $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

      return  json_encode($allfeatures, JSON_PRETTY_PRINT);
     }
}